

clear all;
clc

% read spectrum

dat_stack=zeros(1,2);
T=zeros(1,2);
loweventcounter=0;
higheventcounter=0;
ratio=0;
diff=0;
ratio1=0;
ratio2=0;
counter=0;
S=0;

% data for energy binning

mass=9.109e-31;
eV=1.62e-19;
drift=0.55;
lightpeakindex=4068;

% set counter for FEL shot counting and path for files
counter_empty=0;



%dir=['G:\TS-Mar2011\Apr02\electrons']

%for k=602:745
%dir_folder=['G:\TS-Mar2011\Apr02\electrons\' num2str(k) ]

%   A_folder=exist(dir_folder , 'dir');
    % if(A_folder==7)
    %cd(dir_folder);
for i=6679379:6679379
    tic;
    counter=counter+1;

    loweventcounter=0;
    higheventcounter=0;

    % file ID and check if file exists

%fid=[dir 'C2_' num2str(i) '_optdel_plus_0_8_ps_00000.trc'];
fid=['C2_6679379_optdel_plus_0_8_ps_00000.trc'];

     if(exist(fid))
waveform1=ReadLeCroyBinaryWaveform(fid);


% discard negative time values
zeroindex=find(waveform1.x>0);
waveform1.x=waveform1.x(zeroindex);
waveform1.y=waveform1.y(zeroindex);

% % plot waveform "positive"                    % do not plot (29.07)
 figure
 plot(waveform1.x,waveform1.y);

% create mass axis
mz=1:1:length(waveform1.x);
mz=mz';

% invert waveform to be positive
waveform1.y=-1.*waveform1.y;

data_cov(:,counter)=waveform1.y(3800:6500);

      end


% find peaks with defaults
[pks,locs] = findpeaks(waveform1.y,'MINPEAKHEIGHT',0.015,'MINPEAKDISTANCE',10, 'DOUBLESIDED');


% % plot waveform "positive"            % do not plot (29.07)
% figure
% plot(mz,waveform1.y); hold on;
%
% % offset values of peak heights for plotting
% plot(mz(locs),pks+0.01,'k^','markerfacecolor',[1 0 0]);
% xlim([4000 6000]);

% just in case there are no events are found otherwise error will be
% generated

TF=isempty(pks);
if  TF==1
   pks=0;
   locs=0;
end


% offset values of peak heights for plotting

T=horzcat(pks,locs);
S=S+length(T);
T=T;

% store peak data in two column vector
dat_stack = vertcat(dat_stack,T);

% plot(waveform1.x(locs),pks-0.01,'k^','markerfacecolor',[1 0 0]);
% light_peak=sum(waveform.x(7800:9000))
% for n=1:length(locs);
%
%                 if ((locs(n)>9000)&(locs(n)<12000)&(pks))
%                     if (pks>0.4)
%
%                 higheventcounter=higheventcounter+2;
%                     else
%                     higheventcounter=higheventcounter+1;
%                     end
%                 end
%             if ((locs(n)>13000)&(locs(n)<30000))
%                 if (pks>0.4)
%                 loweventcounter=loweventcounter+2;
%                 else
%                     loweventcounter=loweventcounter+1;
%                 end
%             end
%
% end
    % plot data
%     z1=abs(random('Normal',0,1));
%     z2=abs(random('Normal',0,1));
%     ratio1(counter)=higheventcounter+z1;
%     ratio2(counter)=loweventcounter+z2;
% %  ratio2(counter)=light_peak+z2;
%     refreshdata(h,'caller') % Evaluate y in the function workspace
% 	 drawnow; pause(.01)
%     tElapsed=toc

% plot histogram

nbins=length(waveform1.x)./2;
[n,xout]=hist(dat_stack(:,2),nbins);


end

dat_stack_corr=dat_stack(:,2);
dat_stack_corr(dat_stack_corr<4080)=[];

dat_stack=dat_stack_corr;
% plot histogram
% nbins=length(waveform1.x)./2;
% figure;
% [n,xout]=hist(dat_stack(:,2),nbins);
% bar(xout,n);

total_light_peak=length(dat_stack)
total=length(dat_stack_corr)

%%%%%%%%%%%%%%%%
%energy binning%
%%%%%%%%%%%%%%%%

dat_stack=dat_stack_corr;
scop_reso=waveform.x(2)-waveform.x(1);
timebinwidth=2;
timebins=20000; % max datapoints
timestep=scop_reso*timebinwidth;
timemax=timebins*timestep;
timex=0:timestep:timemax;
timey(1:length(timex))=0;


eVstep=1;
eVbins=1000;
eVmax=eVbins*eVstep;
eVx=0:eVstep:eVmax;
eVy(1:length(eVx))=0;
         for m=1:length(dat_stack(:,1));
             time = (dat_stack(m,1)-lightpeakindex)*scop_reso; % in scope units
             if (time>0)
                 timeindex = floor(time/timestep)+1;
                 if timeindex<timebins;
                    timey(timeindex) = timey(timeindex)+1;
                 end
                 % eVspectrum
                 energy = 1/eV*(0.5*mass*(drift/time)*(drift/time)); %  delay from trigger to lightpeak, please check

                 eVindex = floor(energy/eVstep)+1;
                 if eVindex<eVbins;
                     eVy(eVindex) = eVy(eVindex)+1;
                 end

             end

         end
figure;
plot(eVx,eVy);
xlim([0 100])
figure;
plot(timex,timey);


% fid = fopen('test_hist_03.txt','a+');
% fwrite(fid,dat_stack,'int32');
