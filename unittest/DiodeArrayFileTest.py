""" Test module for the DiodeArrayFile class.

    @author : CFG
    @institution : XFEL
    @creation 20151026

"""
import paths
import unittest

import numpy

# Import the class to test.
from DiodeArrayFile import DiodeArrayFile


class DiodeArrayFileTest(unittest.TestCase):
    """
    Test class for the DiodeArrayFile class.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """

    @classmethod
    def tearDownClass(cls):
        """ Tearing down the test class. """

    def setUp(self):
        """ Setting up a test. """

    def tearDown(self):
        """ Tearing down a test. """

    def testConstruction(self):
        """ Testing the construction of a DiodeArrayFile object. """

        diode_array_file = DiodeArrayFile('TestFiles/diode_array_run01.dat')

        self.assertIsInstance(diode_array_file, DiodeArrayFile)

    def testQueries(self):
        """ Test the query methods. """

        diode_array_file = DiodeArrayFile('TestFiles/diode_array_run01.dat')

        # Get data an and mean data.
        data = diode_array_file.getData()
        avg = diode_array_file.getMeanData()

        # Calculate mean data by hand.
        avg_from_data = numpy.sum(data, axis=0) / data.shape[0]

        # Check they are equal.
        self.assertAlmostEqual(sum(avg - avg_from_data), 0.0, 4)

    def testRMS(self):
        """ Check the calculation of the standard deviation (rms) of the data across
        all samples. """
        diode_array_file = DiodeArrayFile('TestFiles/diode_array_run01.dat')

        # Get data, mean, and std.
        data = diode_array_file.getData()
        avg = diode_array_file.getMeanData()
        rms = diode_array_file.getRMS()

        # Calculate std by hand.
        rms_from_data = [(d - avg)**2 for d in data]
        rms_from_data = numpy.array(rms_from_data)
        rms_from_data = numpy.sum(rms_from_data, axis=0) / rms_from_data.shape[0]
        rms_from_data = numpy.sqrt(rms_from_data)

        # Check for consistency.
        self.assertAlmostEqual(sum(rms_from_data - rms), 0.0, 4)





if __name__ == '__main__':
    unittest.main()

