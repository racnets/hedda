""" Test module for the ScopeFile class.

    @author : CFG
    @institution : XFEL
    @creation 20151024

"""
import paths
import unittest

import os

# Import the class to test.
from ScopeFile import ScopeFile


class ScopeFileTest(unittest.TestCase):
    """
    Test class for the ScopeFile class.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """
        cls.testfile_path = os.path.join(os.path.dirname(__file__), 'TestFiles', 'C3_4d3nm_001_00001.trc')

    @classmethod
    def tearDownClass(cls):
        """ Tearing down the test class. """

    def setUp(self):
        """ Setting up a test. """

    def tearDown(self):
        """ Tearing down a test. """

    def notestConstruction(self):
        """ Testing the construction of the ScopeFile instance. """
        scope_file = ScopeFile(self.testfile_path)

        self.assertIsInstance(scope_file, ScopeFile)

    def testQueries(self):
        """ Test that query methods. """

        scope_file = ScopeFile(self.testfile_path)

        x_values = scope_file.x
        y_values = scope_file.y
        info = scope_file.info


if __name__ == '__main__':
    unittest.main()

