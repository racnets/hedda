# -*- coding: utf-8 -*-
"""
Displays CCD data, histogram and lineout.

"""
import os
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import paths
from HedSPE import HedSPE
from Plotter import Plotter

# Open spe file and get a frame.
path = os.path.join('TestFiles', 'SPE_500_frames.SPE')
spe = HedSPE(path)

# Init the App.
pg.mkQApp()

win = pg.GraphicsLayoutWidget()
win.setWindowTitle('CCD Image ')

# A plot area (ViewBox + axes) for displaying the image
frame_plot = win.addPlot()

# Item for displaying image data
frame_img = pg.ImageItem()

frame_plot.addItem(frame_img)

# Custom ROI for selecting an image region
roi = pg.ROI([0, 0], [100, 10])
roi.addScaleHandle([1.0, 1.0], [0.0, 0.0])                   # (0,1)    (0.5,1.0)       (1,1)
frame_plot.addItem(roi)
roi.setZValue(1000)  # make sure ROI is drawn above image

# Isocurve drawing
#iso = pg.IsocurveItem(level=0.8, pen='g')
#iso.setParentItem(img)
#iso.setZValue(5)

# Contrast/color control
frame_hist = pg.HistogramLUTItem()
frame_hist.setImageItem(frame_img)
win.addItem(frame_hist)

# Draggable line for setting isocurve level
#isoLine = pg.InfiniteLine(angle=0, movable=True, pen='g')
#hist.vb.addItem(isoLine)
#hist.vb.setMouseEnabled(y=False) # makes user interaction a little easier
#isoLine.setValue(0.8)
#isoLine.setZValue(1000) # bring iso line above contrast controls

# Another plot area for displaying ROI data
win.nextRow()
lineout_plot = win.addPlot(colspan=1)
lineout_plot.setMaximumHeight(250)
win.resize(1024, 768)
win.show()


# Generate image data
frame_index = 0
frame_data = spe.data[frame_index].transpose()
frame_img.setImage(frame_data)

frame_plot.autoRange()
frame_hist.setLevels(frame_data.min(), frame_data.max())

# build isocurves from smoothed data
#iso.setData(pg.gaussianFilter(data, (2, 2)))

# set position and scale of image
#img.scale(0.2, 0.2)
#img.translate(-50, 0)

# zoom to fit image.

win.nextRow()
avg_plot = win.addPlot()
avg_img = pg.ImageItem()
avg_plot.addItem(avg_img)

avg_roi = pg.ROI([0, 0], [100, 10])
avg_roi.addScaleHandle([1.0, 1.0], [0.0, 0.0])                   # (0,1)    (0.5,1.0)       (1,1)
avg_plot.addItem(avg_roi)
avg_roi.setZValue(1000)  # make sure ROI is drawn above image

avg_hist = pg.HistogramLUTItem()
avg_hist.setImageItem(avg_img)
win.addItem(avg_hist)

# Another plot area for displaying ROI data
win.nextRow()
avg_lineout_plot = win.addPlot(colspan=1)
avg_lineout_plot.setMaximumHeight(250)

avg_data = spe.getMeanData().transpose()
avg_img.setImage(avg_data)

avg_plot.autoRange()
avg_hist.setLevels(avg_data.min(), avg_data.max())

# Callbacks for handling user interaction
def updatePlot():
    global frame_img, avg_img, roi, frame_roi, frame_data, avg_data, lineout_plot, avg_lineout_plot
    frame_selected = roi.getArrayRegion(frame_data, frame_img)
    avg_selected = avg_roi.getArrayRegion(avg_data, avg_img)
    lineout_plot.plot(frame_selected.mean(axis=1), clear=True)
    avg_lineout_plot.plot(avg_selected.mean(axis=1), clear=True)

roi.sigRegionChanged.connect(updatePlot)
avg_roi.sigRegionChanged.connect(updatePlot)
updatePlot()

#def updateIsocurve():
    #global isoLine, iso
    #iso.setLevel(isoLine.value())

#isoLine.sigDragged.connect(updateIsocurve)


## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

