""" Test module for the Scope class.

    @author : CFG
    @institution : XFEL
    @creation 20151024

"""
import paths
import unittest

import os

# Import the class to test.
from Scope import ScopeWidget
from ScopeFile import ScopeFile

from pyqtgraph.Qt import QtGui

class ScopeTest(unittest.TestCase):
    """
    Test class for the Scope class.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """

        cls.testfile_path = os.path.join('TestFiles', 'Scope', 'run_000002')
        scope_files = os.listdir(cls.testfile_path)
        scope_files = [os.path.join(cls.testfile_path, sf) for sf in scope_files if sf.split('.')[-1] == 'trc' ]
        cls.scope_files = [ScopeFile(sf) for sf in scope_files if sf.split('.')[-1] == 'trc' ]

        app = QtGui.QApplication([])
        cls.widget = ScopeWidget(cls.scope_files)

    @classmethod
    def tearDownClass(cls):
        """ Tearing down the test class. """

    def setUp(self):
        """ Setting up a test. """

    def tearDown(self):
        """ Tearing down a test. """

    def testUpdate(self):
        """ Testing the construction of the Scope instance. """

        self.widget.update()

if __name__ == '__main__':
    unittest.main()

