""" Test module for the Plotter module.

    @author : CFG
    @institution : XFEL
    @creation 20151013

"""
import paths
import unittest
import numpy


# Import the class to test.
from Plotter import Plotter


class PlotterTest(unittest.TestCase):
    """
    Test class for the Plotter class.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """

    @classmethod
    def tearDownClass(cls):
        """ Tearing down the test class. """

    def setUp(self):
        """ Setting up a test. """

    def tearDown(self):
        """ Tearing down a test. """

    def testConstruction(self):
        """ Testing the construction of a plotter instance. """
        plotter = Plotter(data=None)

        self.assertIsInstance(plotter, Plotter)

    def testPlotImage(self):
        """ Check that plotting a 2D image works. """
        data = numpy.random.rand(1024, 1024)
        plotter = Plotter(data=data)
        plotter.plotImage()

    def testPlotXY(self):
        """ Check the plotting of x-y data. """

        x = numpy.arange(1024)
        y = numpy.random.rand(1024)

        plotter = Plotter(data = [x,y])
        plotter.plotXY()

    def testAddData(self):
        """ Check that adding new data works. """
        x0 = numpy.arange(1024)
        y0 = numpy.random.rand(1024)
        x1 = numpy.arange(1240)
        y1 = numpy.exp(-x1*x1/50000.)

        plotter = Plotter([x0, y0])
        plotter.addData([x1, y1])

        plotter.plotXY()


if __name__ == '__main__':
    unittest.main()

