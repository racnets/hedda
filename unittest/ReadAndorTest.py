import unittest

import paths
import andor

class ReadAndorTest(unittest.TestCase):
    """ Test class for the reading of Andor sif files."""

    def testReading(self):
        """ Test that opening and reading in a Andor sif file works. """

        sif_file = andor.SifFile(path="../DummyFiles/10sec_1x_bkgd2.sif")


