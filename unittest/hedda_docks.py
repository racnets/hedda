import numpy as np
import os, sys

import paths

# Import third party libs.
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
from pyqtgraph.dockarea import *


from HedSPE import HedSPE
from ScopeFile import ScopeFile
from DiodeArrayFile import DiodeArrayFile

import platform

###################
# Find out the platform.
is_win    = platform.system() == 'Windows'
is_linux  = platform.system() == 'Linux'
is_macosx = platform.system() == 'Darwin'

# Find out if 32/64 bit.
is_32bit  = platform.architecture()[0] == '32bit'


basepath = os.path.dirname(__file__)
testfilespath = os.path.join(basepath, 'TestFiles')
spe = HedSPE(os.path.join(testfilespath, 'SPE_500_frames.SPE'))
scope_file = ScopeFile(os.path.join(testfilespath, 'C3_4d3nm_001_00001.trc') )
diode_array_file = DiodeArrayFile( os.path.join(testfilespath, 'diode_array_run01.dat') )

print "Read data ok."

if is_linux:
    print "Starting QApp on linux."
    app = QtGui.QApplication([])

win = QtGui.QMainWindow()
area = DockArea()
win.setCentralWidget(area)
win.setWindowTitle('HEDDA Workspace')

# Create docks.
d1 = Dock("CCD 1", closable=True)
d2 = Dock("Scope", closable=True)
d3 = Dock("DiodeArray", closable=True)
area.addDock(d1, 'left')
area.addDock(d2, 'above', d1)
area.addDock(d3, 'above', d2)

# Add widgets into each dock
######################################
#    CCD widget                      #
######################################
ccd_widget = QtGui.QWidget()
layout = QtGui.QGridLayout()
ccd_widget.setLayout(layout)

frames_view = pg.ImageView(view=pg.PlotItem(title="Frame"))
frames_lineout_widget = pg.PlotWidget(name='frames_lineout_widget', title="Lineout")
avg_view = pg.ImageView(view=pg.PlotItem(title="Frame mean"))

layout.addWidget(frames_view, 0, 0)
layout.addWidget(avg_view, 0, 1)
layout.addWidget(frames_lineout_widget, 2,0,1,2)
layout.setRowMinimumHeight(0, 500)

data = np.array([d.transpose() for d in spe.data])
avg = spe.getMeanData().transpose()

## Display the data and assign each frame a time value from 1.0 to 3.0
frames_view.setImage(data, xvals=np.arange(data.shape[0]))
avg_view.setImage(avg)

frame_roi = frames_view.roi
#frame_roi.setSize(size=(data.shape[0]))

def updateLineouts():
    #global

    index = frames_view.currentIndex
    frame_data = frames_view.getProcessedImage()[index]
    frame_image = frames_view.getImageItem()
    frame_selected = frame_roi.getArrayRegion(frame_data, frame_image)
    avg_selected = frame_roi.getArrayRegion(avg, frame_image)
    frames_lineout_widget.plot(frame_selected.mean(axis=1), clear=True)
    frames_lineout_widget.plot(avg_selected.mean(axis=1), clear=False)


frame_roi.sigRegionChanged.connect(updateLineouts)
frames_view.sigTimeChanged.connect(updateLineouts)
#avg_roi.sigRegionChanged.connect(updatePlot)
updateLineouts()

# Set ROI to 100x10 window.
frame_roi.setPos(pos=(0,0))
frame_roi.setSize(size=(100, 10))

# Add scale handles on all corners and edges.
frame_roi.addScaleHandle([0.5, 0.0], [0.5, 1.0] )
frame_roi.addScaleHandle([0.0, 0.5], [1.0, 0.5] )
frame_roi.addScaleHandle([0.5, 1.0], [0.5, 0.0] )
frame_roi.addScaleHandle([1.0, 0.5], [0.0, 0.5] )
frame_roi.addScaleHandle([0, 0], [1, 1])
frame_roi.addScaleHandle([0, 1], [1, 0])
frame_roi.addScaleHandle([1, 0], [0, 1])


d1.addWidget(ccd_widget)

################################
#    Scope widget              #
################################
## Add widgets into each dock
scope_widget = QtGui.QWidget()
scope_layout = QtGui.QGridLayout()
scope_widget.setLayout(scope_layout)

scope_xy_plot_widget = pg.PlotWidget(name='scope_xy_plot_widget', title="Scope X-Y")

scope_layout.addWidget(scope_xy_plot_widget, 0,0)


## Create random 3D data set with noisy signals
x = scope_file.x
y = scope_file.y
info = scope_file.info

scope_xy_plot_widget.addItem(pg.PlotDataItem(x,y))
scope_xy_plot_widget.plot()

d2.addWidget(scope_widget)


####################################
#      DiodeArray widget           #
####################################
diode_array_widget = QtGui.QWidget()
diode_array_layout = QtGui.QGridLayout()
diode_array_widget.setLayout(diode_array_layout)

diode_array_plot_widget = pg.PlotWidget(name='diode_array_plot_widget', title='Diode Array')

diode_array_layout.addWidget(diode_array_plot_widget, 0,0)

diode_array_data = diode_array_file.data
diode_array_mean = diode_array_file.getMeanData()
diode_array_rms = diode_array_file.getRMS()

diode_array_mean_plus_rms = diode_array_mean + diode_array_rms
diode_array_mean_minus_rms = diode_array_mean - diode_array_rms

x = np.arange(diode_array_data.shape[1])

diode_array_plot_widget.addItem( pg.PlotDataItem(x, diode_array_mean) )
diode_array_plot_widget.addItem( pg.PlotDataItem(x, diode_array_mean_plus_rms) )
diode_array_plot_widget.addItem( pg.PlotDataItem(x, diode_array_mean_minus_rms) )

diode_array_plot_widget.plot()

d3.addWidget(diode_array_widget)

win.resize(1200,900)
win.show()

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

