""" Test module for HedSPE.

    @author : CFG
    @institution : XFEL
    @creation 20151310

"""
import paths
import unittest

from Plotter import Plotter

# Import the class to test.
from HedSPE import HedSPE


class HedSPETest(unittest.TestCase):
    """
    Test class for HedSPE.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """

    @classmethod
    def tearDownClass(cls):
        """ Tearing down the test class. """

    def setUp(self):
        """ Setting up a test. """

    def tearDown(self):
        """ Tearing down a test. """

    def testConstruction(self):
        """ Testing the construction of an HedSPE instance. """

        # Construct the object.
        spe = HedSPE('TestFiles/90-s230.SPE')

        # Check its type.
        self.assertIsInstance(spe, HedSPE)

    def testData(self):
        """ Testing the internal data of an HedSPE instance. """

        # construct the object.
        spe = HedSPE('TestFiles/90-s230.SPE')

        # Get the data.
        spe_data = spe.data

        self.assertEqual( spe_data.shape[0], 1)
        self.assertEqual( spe_data.shape[1], 512 )
        self.assertEqual( spe_data.shape[2], 2048 )

        self.assertEqual( spe.roi.frame_min, 0 )
        self.assertEqual( spe.roi.frame_max, 1 )

        self.assertEqual( spe.roi.x_min, 0 )
        self.assertEqual( spe.roi.x_max, 512 )

        self.assertEqual( spe.roi.y_min, 0 )
        self.assertEqual( spe.roi.y_max, 2048 )

    def testQueries(self):
        """ Check that plotting the data works. """

        # Get a spe.
        spe = HedSPE('TestFiles/CCD1/SPE_500_frames.SPE')

        plotter = Plotter()

        all_frames = spe.data
        binned_frames = spe.getBinnedData()
        plotter.setData(binned_frames)
        plotter.plotImage()

        mean = spe.getMeanData()
        plotter.setData(mean)
        plotter.plotImage()
        #mean_excl = spe.getMean(indices=range(10))

    def testGetRMSPerFrame(self):
        """ Check the RMSPerFrame functionality. """

        spe = HedSPE('TestFiles/SPE_500_frames.SPE')

        rms_per_frame = spe.getRMSPerFrame()

        plotter = Plotter(rms_per_frame)
        plotter.plotXY()

    def testRun(self):
        """ A test run. """

        spe = HedSPE('TestFiles/SPE_500_frames.SPE')

        data = spe.getData()
        plotter=Plotter(data[0]); plotter.plotImage()
        plotter.setData(data[1]); plotter.plotImage()
        plotter.setData(data[100]); plotter.plotImage()
        plotter.setData(data[300]); plotter.plotImage()

    def testSetROI(self):
        """ Test setting the ROI. """

        spe = HedSPE('TestFiles/SPE_500_frames.SPE')
        #spe.setROI(y_min=0 )
        #spe.setROI(y_max=300 )

        print spe.roi.frame_min
        print spe.roi.frame_max

        print spe.roi.x_min
        print spe.roi.x_max

        print spe.roi.y_min
        print spe.roi.y_max

    def testPlotImage(self):
        """ Test plotting the image. """

        spe = HedSPE('TestFiles/SPE_500_frames.SPE')
        spe.setROI(frame_min=10, frame_max=20, y_min=50, y_max=100 )

        spe.plotImage(frame=0)


    def testLineOut(self):
        """ Test the line-out capability. """
        spe = HedSPE('TestFiles/SPE_500_frames.SPE')
        spe.setROI(frame_min=10, frame_max=20, y_min=50, y_max=100 )

        lineout = spe.lineOut(direction='x', frame=0)


    def testPlotLineOut(self):
        """ Test the line-out capability. """

        spe = HedSPE('TestFiles/SPE_500_frames.SPE')
        spe.setROI(frame_min=10, frame_max=20, y_min=50, y_max=100 )

        spe.plotLineOut(direction='x', frame=0)

    def testBackgroundSubtraction(self):
        """ Test the background subtraction. """

        spe = HedSPE('TestFiles/SPE_500_frames.SPE')
        spe.subtractBackgroundFromFrame(100)

if __name__ == '__main__':
    unittest.main()

