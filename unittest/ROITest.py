""" Test module for ROI.

    @author : CFG
    @institution : XFEL
    @creation 20151016

"""
import paths
import unittest


# Import the class to test.
from ROI import ROI
from ROI import checkAndSetMinMax


class ROITest(unittest.TestCase):
    """
    Test class for ROI.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """

    @classmethod
    def tearDownClass(cls):
        """ Tearing down the test class. """

    def setUp(self):
        """ Setting up a test. """

    def tearDown(self):
        """ Tearing down a test. """

    def testConstruction(self):
        """ Testing the construction of the ROI object. """
        roi = ROI(frame_min=0, frame_max=10, x_min=0, x_max=10, y_min=0, y_max=100)

        self.assertIsInstance(roi, ROI)

    def testCheckAndSetMinMax(self):

        # Wrong types
        self.assertRaises(TypeError, checkAndSetMinMax, None, None, '' )
        self.assertRaises(TypeError, checkAndSetMinMax, frame_max='1')
        self.assertRaises(TypeError, checkAndSetMinMax, frame_max=1, frame_min=0)

    def testQueries(self):

        # Check fails if no ranges are given.
        self.assertRaises(RuntimeError, ROI)

        roi = ROI(0,10, 0, 100, 3, 50)
        self.assertEqual(roi.x_min, 0)
        self.assertEqual(roi.x_max, 100)
        self.assertEqual(roi.y_min, 3)
        self.assertEqual(roi.y_max, 50)
        self.assertEqual(roi.frame_min, 0)
        self.assertEqual(roi.frame_max, 10)
if __name__ == '__main__':
    unittest.main()

