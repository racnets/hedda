# -*- coding: utf-8 -*-
"""
Displays CCD data, histogram and lineout.

"""

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import paths
from HedSPE import HedSPE
from Plotter import Plotter

# Open spe file and get a frame.
# Adjust path depending on architecture.
spe = HedSPE('TestFiles/SPE_500_frames.SPE')

import numpy as np
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

app = QtGui.QApplication([])

## Create window with ImageView widget
win = QtGui.QMainWindow()
central_widget = QtGui.QWidget()
win.setCentralWidget(central_widget)

layout = QtGui.QGridLayout()
central_widget.setLayout(layout)

frames_view = pg.ImageView(view=pg.PlotItem(title="Frame"))
frames_lineout_widget = pg.PlotWidget(name='frames_lineout_widget', title="Lineout")
avg_view = pg.ImageView(view=pg.PlotItem(title="Frame mean"))

layout.addWidget(frames_view, 0, 0)
layout.addWidget(frames_lineout_widget, 1,0, 1, 2)
layout.addWidget(avg_view, 0, 1)
layout.setRowMinimumHeight(0,600)

win.resize(1024,768)
win.show()
win.setWindowTitle('Shot analyzer')

## Create random 3D data set with noisy signals
data = np.array([d.transpose() for d in spe.data])
avg = spe.getMeanData().transpose()

## Display the data and assign each frame a time value from 1.0 to 3.0
frames_view.setImage(data, xvals=np.arange(data.shape[0]))
avg_view.setImage(avg)

frame_roi = frames_view.roi

def updateLineouts():
    #global

    index = frames_view.currentIndex
    frame_data = frames_view.getProcessedImage()[index]
    frame_image = frames_view.getImageItem()
    frame_selected = frame_roi.getArrayRegion(frame_data, frame_image)
    avg_selected = frame_roi.getArrayRegion(avg, frame_image)
    frames_lineout_widget.plot(frame_selected.mean(axis=1), clear=True)
    frames_lineout_widget.plot(avg_selected.mean(axis=1), clear=False)


frame_roi.sigRegionChanged.connect(updateLineouts)
frames_view.sigTimeChanged.connect(updateLineouts)
#avg_roi.sigRegionChanged.connect(updatePlot)
updateLineouts()


## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

