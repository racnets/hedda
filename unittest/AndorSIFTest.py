""" Test module for AndorSIF.

    @author : CFG
    @institution : XFEL
    @creation 20151310

"""
import paths
import unittest

import andor

# Import the class to test.
from AndorSIF import AndorSIF


class AndorSIFTest(unittest.TestCase):
    """
    Test class for AndorSIF.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """

    @classmethod
    def tearDownClass(cls):
        """ Tearing down the test class. """

    def setUp(self):
        """ Setting up a test. """

    def tearDown(self):
        """ Tearing down a test. """

    def testConstruction(self):
        """ Testing the construction of an AndorSIF instance. """

        # Construct the object.
        sif = AndorSIF('TestFiles/10sec_1x_bkgd2.sif')

        # Check its type.
        self.assertIsInstance(sif, AndorSIF)

    def testData(self):
        """ Testing the internal data of an AndorSIF instance. """

        # Construct the object.
        sif = AndorSIF('TestFiles/10sec_1x_bkgd2.sif')

        # Get the data.
        sif_data = sif.data

        self.assertEqual( sif_data.shape[0], 1024 )
        self.assertEqual( sif_data.shape[1], 1024 )

    def testPlotting(self):
        """ Check that plotting the data works. """

        # Get a sif.
        sif = AndorSIF('TestFiles/10sec_1x_bkgd2.sif')

        sif.plotImage()


if __name__ == '__main__':
    unittest.main()

