""" Test module for StatisticsUtilities.

    @author : CFG
    @institution : XFEL
    @creation 20151203

"""
import paths
import numpy
import random
import unittest

# Import the class to test.
from StatisticsUtilities import statistics


class StatisticsUtilitiesTest(unittest.TestCase):
    """
    Test class for StatisticsUtilities.
    """

    @classmethod
    def setUpClass(cls):
        """ Setting up the test class. """

    @classmethod
    def tearDownClass(cls):
        """ Tearing down the test class. """

    def setUp(self):
        """ Setting up a test. """

    def tearDown(self):
        """ Tearing down a test. """

    def testGauss0(self):
        """ Testing statistics over Gaussians of same width, mean, and intensity."""
        test_data = numpy.empty((32, 80, 60)) # 32 frames of 80x60 pixels each.

        framel, xl, yl = test_data.shape

        frames = range(framel)
        ys = range(yl)
        xs = range(xl)

        xmean = xl/2.
        xwidt = xl/10.
        ymean = yl/2.
        ywidt = yl/10.

        for frame in frames:
            for y in ys:
                for x in xs:
                    test_data[frame,x,y] = numpy.exp(-(x-xmean)**2 / 2. / xwidt**2 - (y-ymean)**2 / 2. / ywidt**2 )

        stat = statistics(test_data)

        for key in ['mean_pixel_x',
                    'rms_pixel_x',
                    'mean_pixel_y',
                    'rms_pixel_y',
                    'mean_width_x',
                    'rms_width_x',
                    'mean_width_y',
                    'rms_width_y',
                    'mean_intensity',
                    'rms_intensity',
                    ]:

            print key + '\t%6.4f' % (  stat[key] )

    def testGauss1(self):
        """ Testing statistics over Gaussians of same width, random mean, and same intensity."""
        test_data = numpy.empty((32, 80, 60)) # 32 frames of 80x60 pixels each.

        framel, xl, yl = test_data.shape

        frames = range(framel)
        ys = range(yl)
        xs = range(xl)

        xwidt = xl/10.
        ymean = yl/2.
        ywidt = yl/10.

        for frame in frames:
            xmean = random.randint(xl/2-10, xl/2+10)
            for y in ys:
                for x in xs:
                    test_data[frame,x,y] = numpy.exp(-(x-xmean)**2 / 2. / xwidt**2 - (y-ymean)**2 / 2. / ywidt**2 )

        stat = statistics(test_data)

        for key in ['mean_pixel_x',
                    'rms_pixel_x',
                    'mean_pixel_y',
                    'rms_pixel_y',
                    'mean_width_x',
                    'rms_width_x',
                    'mean_width_y',
                    'rms_width_y',
                    'mean_intensity',
                    'rms_intensity',
                    ]:

            print key + '\t%6.4f' % (  stat[key] )


if __name__ == '__main__':
    unittest.main()

