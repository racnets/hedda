"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.

"""

import paths
import numpy
import platform


# Import third party libs.
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui


class CCD2ControlWidget(QtGui.QWidget):


    def __init__(self,**kwargs):

        super(CCD2ControlWidget, self).__init__()

        layout = QtGui.QGridLayout()
        self.setLayout( layout)

        # Add on parent layout.
        layout.addWidget(QtGui.QLabel(u'CCD 40^o'), 0, 0)

        # Spin boxes for ROI control.
        self.xmin_spin = pg.SpinBox(value=0.0, bounds=[0, None], step=1)
        self.xmax_spin = pg.SpinBox(value=10.0, bounds=[0, None], step=1)
        self.ymin_spin = pg.SpinBox(value=0.0, bounds=[0, None], step=1)
        self.ymax_spin = pg.SpinBox(value=10.0, bounds=[0, None], step=1)

        layout.addWidget(QtGui.QLabel('X min'), 1, 0)
        layout.addWidget(QtGui.QLabel('X max'), 1, 1)
        layout.addWidget(self.xmin_spin, 2, 0)
        layout.addWidget(self.xmax_spin, 2, 1)
        layout.addWidget(QtGui.QLabel('Y min'), 3, 0)
        layout.addWidget(QtGui.QLabel('Y max'), 3, 1)
        layout.addWidget(self.ymin_spin, 4, 0)
        layout.addWidget(self.ymax_spin, 4, 1)

        # Button to get statistics.
        self.statistics_button = QtGui.QPushButton('Update CCD2')
        layout.addWidget(self.statistics_button, 0, 2)

        # Data table for CCD 2 Mie ROI statistics.
        self.data_table_widget_1 = pg.TableWidget()
        layout.addWidget(self.data_table_widget_1, 1, 2, 10, 3)

        table_data_1 = numpy.array([
                ('X mean', 0.0),
                ('X mean rms', 0.0),
                ('Y mean', 0.0),
                ('Y mean rms', 0.0),
                ('X width', 0.0),
                ('X width rms', 0.0),
                ('Y width', 0.0),
                ('Y width rms', 0.0),
                ], dtype=[('Observable', object), ('Value', float)])


        self.data_table_widget_1.setData(table_data_1)

        # Data table for CCD 2 Thomson ROI statistics.
        self.data_table_widget_2 = pg.TableWidget()
        layout.addWidget(self.data_table_widget_2, 1, 5, 10, 3)

        table_data_2 = numpy.array([
                ('X mean', 0.0),
                ('X mean rms', 0.0),
                ('Y mean', 0.0),
                ('Y mean rms', 0.0),
                ('X width', 0.0),
                ('X width rms', 0.0),
                ('Y width', 0.0),
                ('Y width rms', 0.0),
                ], dtype=[('Observable', object), ('Value', float)])


        self.data_table_widget_2.setData(table_data_2)


        # Spin boxes for ROI control.
        self.xmin_spin_2 = pg.SpinBox(value=0.0, bounds=[0, None], step=1)
        self.xmax_spin_2 = pg.SpinBox(value=10.0, bounds=[0, None], step=1)
        self.ymin_spin_2 = pg.SpinBox(value=0.0, bounds=[0, None], step=1)
        self.ymax_spin_2 = pg.SpinBox(value=10.0, bounds=[0, None], step=1)


        self.ymin_spin_2.setEnabled(False)
        self.ymax_spin_2.setEnabled(False)

        layout.addWidget(QtGui.QLabel('X min'), 6, 0)
        layout.addWidget(QtGui.QLabel('X max'), 6, 1)
        layout.addWidget(self.xmin_spin_2, 7, 0)
        layout.addWidget(self.xmax_spin_2, 7, 1)
        layout.addWidget(QtGui.QLabel('Y min'), 8, 0)
        layout.addWidget(QtGui.QLabel('Y max'), 8, 1)
        layout.addWidget(self.ymin_spin_2, 9, 0)
        layout.addWidget(self.ymax_spin_2, 9, 1)


