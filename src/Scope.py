"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""

import paths
import numpy
import itertools


# Import third party libs.
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

from peakdetect import peakdetect



class ScopeWidget(QtGui.QWidget):

    def __init__(self, scope_files):



        super(ScopeWidget, self).__init__()

        if scope_files is not None:
            self.setFiles(scope_files)

        layout = QtGui.QGridLayout()

        self.scope_xy_plot_widget = pg.PlotWidget(name='scope_xy_plot_widget', title="Scope X-Y")
        self.time_histogram_widget = pg.PlotWidget(name='time_histogram_widget', title="Time histogram")
        self.energy_histogram_widget = pg.PlotWidget(name='energy_histogram_widget', title="Energy histogram")

        layout.addWidget(self.scope_xy_plot_widget, 0,0)
        layout.addWidget(self.time_histogram_widget, 0,1)
        layout.addWidget(self.energy_histogram_widget, 1,0)

        self.setLayout(layout)


        # Get parameters from control widget.
        self.parameters = {'light_peak_index' : 4067,
                           'drift'            : 0.55,
                           'energy_max'       : 1000.0,
                           'energy_binning'   : 1.0,
                           'time_max'         : 5.0e-6,
                           'time_binning'     : 1.0e-8
                           }

        self.__loaded = False

    def setFiles(self, scope_files):
        self.scope_files = scope_files
        self.__loaded = False

    def loadData(self):
        """
        Load the data from files into memory.
        """

        if not self.__loaded:
            self.xy_data = [[f.x, -f.y]  for  f in self.scope_files]
            self.__loaded = True
        else:
            pass

    def update(self):


        # Get x and y data.
        if not self.__loaded:
            self.loadData()


        # Clear all plots.
        self.scope_xy_plot_widget.clear()
        self.time_histogram_widget.clear()
        self.energy_histogram_widget.clear()


        time_max = self.parameters['time_max']

        light_peak_index=self.parameters['light_peak_index']

        # Skip negative times.
        # Assuming common x values for all traces.
        x = self.xy_data[0][0]
        positives = numpy.where(x>0.0)
        low_pass = numpy.where(x<=time_max)
        x = x[positives]
        x = x[low_pass]

        y = numpy.array([yy[1][positives][low_pass] for yy in self.xy_data])

        times = x - x[light_peak_index]
        summed_waveform = numpy.sum(y, axis=0)
        self.scope_xy_plot_widget.addItem(pg.PlotDataItem(times,summed_waveform))

        self.scope_xy_plot_widget.plot()

        # Get the peaks
        self.peak_finder = PeakFinderThread(x, y)
        self.peak_finder.finished.connect(self.updateHistograms)
        self.peak_finder.start()
        
    def updateHistograms(self):
        time_max = self.parameters['time_max']
        time_binning = self.parameters['time_binning']
        energy_max = self.parameters['energy_max']
        energy_binning = self.parameters['energy_binning']
        light_peak_index=self.parameters['light_peak_index']
        drift = self.parameters['drift']

        x = self.xy_data[0][0]
        positives = numpy.where(x>0.0)
        low_pass = numpy.where(x<=time_max)
        x = x[positives]
        x = x[low_pass]
        max_pos = [[m[0] for m in mp] for mp in self.peak_finder.peaks]
        max_pos = list( itertools.chain.from_iterable( max_pos ) )
        
        # Get binning.
        number_of_time_bins = int(numpy.floor(time_max/time_binning))
        number_of_energy_bins = int(numpy.floor(energy_max/energy_binning))
        time_histogram = numpy.histogram(max_pos, bins=number_of_time_bins, density=False)

        light_peak_time = x[light_peak_index]

        electron_mass=9.109e-31
        electron_charge=1.62e-19
        inverse_second_to_eV = 0.5*electron_mass/electron_charge
        energies = numpy.array([inverse_second_to_eV*(drift/(m-light_peak_time))**2 for m in max_pos if m>light_peak_time])

        energies = energies[numpy.where(energies < energy_max)]

        energy_histogram = numpy.histogram(energies, bins=number_of_energy_bins, density=False)

        self.time_histogram_widget.addItem(pg.PlotDataItem(time_histogram[1], time_histogram[0], stepMode=True, fillLevel=0))
        self.time_histogram_widget.plot()

        self.energy_histogram_widget.addItem(pg.PlotDataItem(energy_histogram[1], energy_histogram[0], stepMode=True, fillLevel=0))
        self.energy_histogram_widget.plot()


    def lightPeakIndexChanged(self, control_widget):
        """ Receives the "valChanged" signals to trigger plot update"""
        self.receiveParameters(control_widget, 'light_peak_index')
        self.update()

    def driftChanged(self, control_widget):
        """ Receives the "valChanged" signals to trigger plot update"""
        self.receiveParameters(control_widget, 'drift')
        self.update()

    def energyMaxChanged(self, control_widget):
        """ Receives the "valChanged" signals to trigger plot update"""
        self.receiveParameters(control_widget, 'energy_max')
        self.update()

    def energyBinningChanged(self, control_widget):
        """ Receives the "valChanged" signals to trigger plot update"""
        self.receiveParameters(control_widget, 'energy_binning')
        self.update()

    def timeMaxChanged(self, control_widget):
        """ Receives the "valChanged" signals to trigger plot update"""
        self.receiveParameters(control_widget, 'time_max')
        self.update()

    def timeBinningChanged(self, control_widget):
        """ Receives the "valChanged" signals to trigger plot update"""
        self.receiveParameters(control_widget, 'time_binning')
        self.update()


    def receiveParameters(self, control_widget, key):

        val = control_widget.value()
        self.parameters[key] = val


class PeakFinderThread(QtCore.QThread):
    def __init__(self, x, y, parent=None):
        QtCore.QThread.__init__(self)

        self.x = x
        self.y = y

    def run(self):
        self.peaks = getPeaks(self.x, self.y)
        

def getPeaks(x, y):
    return [peakdetect.peakdetect(y[i], x, lookahead=10, delta=0.015)[0] for i in range(len(y))]

