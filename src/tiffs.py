import numpy
import os
from matplotlib import image, pylab

path_to_tiffs = "F:/TS_data_dec_2015/1207_02h30_run1/CCD2/tiff"

log = os.path.join(path_to_tiffs, 'log.txt')

tiffs = [os.path.join(path_to_tiffs, tif) for tif in os.listdir(path_to_tiffs) if 'tif' in tif]

number_of_images = len(tiffs)

pics = []

for tif in tiffs:
    im = image.imread(tif).transpose()
    pics.append(im)
    
