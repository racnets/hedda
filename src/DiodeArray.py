"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""
import paths

import numpy

# Import third party libs.
import pyqtgraph as pg
from pyqtgraph.Qt import  QtGui


class DiodeArrayWidget(QtGui.QWidget):

    def __init__(self, diode_array_file=None):

        # Init base class.
        super( DiodeArrayWidget, self).__init__()

        ####################################
        #      DiodeArray widget           #
        ####################################
        diode_array_layout = QtGui.QGridLayout()
        self.setLayout(diode_array_layout)

        self.diode_array_plot_widget = pg.PlotWidget(name='diode_array_plot_widget', title='Diode Array')

        diode_array_layout.addWidget(self.diode_array_plot_widget, 0,0)

        if diode_array_file is not None:
            self.setFiles(diode_array_file)
            self.update()

    def setFile(self, file):
        self.diode_array_file = file

    def update(self):
        # Get mean and rms over all shots in this run.

        # First clear the plots.
        self.diode_array_plot_widget.clear()

        diode_array_mean_1, diode_array_mean_2 = self.diode_array_file.getMeanData()
        diode_array_rms_1, diode_array_rms_2 = self.diode_array_file.getRMS()

        # Interleave diodes 1 and 2.
        self.diode_array_mean = numpy.array([[dm1, dm2] for (dm1, dm2) in zip(diode_array_mean_1, diode_array_mean_2)]).flatten()
        self.diode_array_rms = numpy.array([[dm1, dm2] for (dm1, dm2) in zip(diode_array_rms_1, diode_array_rms_2)]).flatten()


        # Get x axis (diode index)
        x = numpy.arange(1,len(self.diode_array_mean)+1)

        # Plot.
        self.diode_array_plot_widget.addLegend()
        err_item = pg.ErrorBarItem(x=x, y=self.diode_array_mean, top=self.diode_array_rms,bottom=self.diode_array_rms, beam=0.3)
        self.diode_array_plot_widget.addItem( pg.PlotDataItem(x, self.diode_array_mean, name='mean', pen=(0,255,0) , symbolPen='w'))
        self.diode_array_plot_widget.addItem( err_item )


        self.diode_array_plot_widget.plot()
