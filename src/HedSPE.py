""" Module for HedSPE

    @author : CFG
    @institution : XFEL
    @creation 20151013


    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""
import os.path
import numpy
from pyspec.ccd.files import PrincetonSPEFile

from ROI import ROI
from ROI import checkAndSetMinMax
from Plotter import Plotter


class HedSPE(PrincetonSPEFile, object):
    """

    """

    def __init__(self, path=None, roi=None, background_frame=None, background_data=None):
        """
        Constructor for the HedSPE object.

        @param path : The path to the spe file.
        @type : string
        @default : None
        """

        # Check if file exists.
        if not os.path.isfile(path):
            raise RuntimeError("File %s not found or not a file." % (path))

        else:
            super(HedSPE, self).__init__(path)

        data_shape = self.getSize()
        if roi is None:
            self.__roi = ROI( frame_min=0, frame_max=data_shape[0],
                              x_min=0, x_max=data_shape[1],
                              y_min=0, y_max=data_shape[2] )
        self.__data = self.getData()

        if background_frame is not None:
            self.subtractBackgroundFromFrame(background_frame)
        if background_data is not None:
            self.subtractBackgroundFromData(background_data)

    @property
    def data(self):
        return self.__data

    @property
    def roi(self):
        return self.__roi

    def setROI(self, frame_min=None, frame_max=None, x_min=None, x_max=None, y_min=None, y_max=None):
        """ Set the ROI using explicit boundaries. Default uses all frames and pixels."""

        nframes, npixx, npixy = self.getSize()
        if frame_min is None:
            frame_min = 0
        if frame_max is None:
            frame_max = nframes
        if x_min is None:
            x_min = 0
        if x_max is None:
            x_max = npixx
        if y_min is None:
            y_min = 0
        if y_max is None:
            y_max = npixy

        frame_min, frame_max = checkAndSetMinMax(frame_min, frame_max, 'frame')
        x_min, x_max = checkAndSetMinMax(x_min, x_max, 'x')
        y_min, y_max = checkAndSetMinMax(y_min, y_max, 'y')

        self.__roi = ROI(frame_min, frame_max, x_min, x_max, y_min, y_max)

    def getMeanData(self):
        """ Return the mean over all frames in the stack. """

        data = self.data
        mean = numpy.sum(data, axis=0) / data.shape[0]

        return mean

    def getRMSPerFrame(self):
        """ Calculate the rms deviation of each frame from the mean over all frames. """

        all_data = self.data
        mean = self.getMeanData()

        all_flat = [ad.flatten() for ad in all_data]
        mean_flat = mean.flatten()

        number_of_frames = len(all_flat)
        number_of_pixels = len(all_flat[0])

        rms = [numpy.sqrt(numpy.sum((af - mean_flat)**2)/number_of_pixels) for af in all_flat]
        frame_indices = numpy.arange(len(rms))

        return [frame_indices, rms]

    def plotImage(self, frame=0):
        """ Convenience function for plotting.
        @param frame : Which frame to plot (0 is the index of the first frame in the ROI)."""

        plotter = Plotter(self.data[frame])

        plotter.plotImage()

    def lineOut(self, frame=None, direction=None):
        """ Project the data along a given direction
        @param frame : The frame for which to do the lineout.
        @default : All frames in the ROI.
        @note : Accepts frame='avg' to return the lineout based on averaged frames.

        @param direction : Direction along which to project the data.
        @default : x

        @return The projected data
        @rtype : 2D numpy array of shape (n_frames, n_pixels)
        """

        # Check and set the direction.
        direction = checkAndSetDirection(direction)

        # Map direction string to axis for summation.
        axis = {'x' : 0,
                'y' : 1}[direction.lower()]

        # Get mean data if requested.
        if frame is 'avg' or None:
            data = self.getMeanData()

        # If frame is given, do operation for that frame.
        else:
            data = self.data[frame]

        # Do the projection.
        projected_data = numpy.sum(data, axis=axis)

        return projected_data

    def plotLineOut(self, direction=None, frame=None):

        direction = checkAndSetDirection(direction)
        avg = self.lineOut(direction=direction, frame='avg')

        if frame is not None:
            data  = self.lineOut(direction=direction, frame=frame)

        plotter=Plotter([avg])
        plotter.addData([data])

        plotter.plotXY()

    def subtractBackgroundFromFrame(self, background_frame):
        """ Subtract the data from the given frame as background in all frames.
        @param background_frame : The frame to subtract.
        """

        # Check if given frame in range.
        if background_frame < 0 or background_frame > self.getSize()[0]:
            raise RuntimeError("Parameter 'background_frame' must be >=0 and <= %d" % (self.getSize()[0] ) )

        bg_data = self.data[background_frame]

        self.__data = numpy.array([d - bg_data for d in self.data])

    def subtractBackgroundFromData(self, background_data):
        """ Subtract the data from the given frame as background in all frames.
        @param background_frame : The frame to subtract.
        """

        # Check if background data has correct shape
        if background_data.shape != self.getMeanData().shape:
            raise RuntimeError("Parameter 'background_data' must have the same shape as the frame data (%s != %s)" % (str(background_data.shape), str(self.getMeanData().shape)) )

        self.__data = numpy.array([d - background_data for d in self.__data])



def checkAndSetDirection(direction):
    """ Check utility for a projection direction. """

    if direction is None:
        direction = 'x'

    if not isinstance(direction, str):
        raise RuntimeError("Parameter 'direction' must be a string.")
    direction = direction.lower()
    if not direction in 'xy':
        raise RuntimeError("Parameter 'direction' must be 'x' or 'y'.")

    return direction
