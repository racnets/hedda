"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""

import paths
import numpy
import platform


# Import third party libs.
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui


class ScopeControlWidget(QtGui.QWidget):


    def __init__(self,**kwargs):

        super(ScopeControlWidget, self).__init__()

        # Define layout.
        layout = QtGui.QGridLayout()
        self.setLayout(layout)

        self.light_peak_index = pg.SpinBox(value=0, bounds=[0, None], step=1, name='light_peak_index')
        self.drift = pg.SpinBox(value=0.55, bounds=[0, None], step=0.01, name='drift')
        self.energy_max = pg.SpinBox(value=1000, bounds=[0, None], step=10, name='energy_max')
        self.energy_binning = pg.SpinBox(value=1.0, bounds=[0.001, self.energy_max.value()], step=0.1, name='energy_binning')
        self.time_max = pg.SpinBox(value=5e-6, bounds=[0, None], step=1e-7, name='time_max')
        self.time_binning = pg.SpinBox(value=1e-8, bounds=[0, None], step=1e-8, name='time_binning')

        layout.addWidget(QtGui.QLabel('Scope'), 0,0)
        layout.addWidget(QtGui.QLabel('Light Peak Index'), 1, 0)
        layout.addWidget(QtGui.QLabel('Drift'), 2, 0)
        layout.addWidget(QtGui.QLabel('E max (eV)'), 3, 0)
        layout.addWidget(QtGui.QLabel('dE (eV)'), 4, 0)
        layout.addWidget(QtGui.QLabel('T max (s)'), 5, 0)
        layout.addWidget(QtGui.QLabel('dT (s)'), 6, 0)

        layout.addWidget(self.light_peak_index, 1, 1)
        layout.addWidget(self.drift, 2, 1)
        layout.addWidget(self.energy_max, 3, 1)
        layout.addWidget(self.energy_binning, 4, 1)
        layout.addWidget(self.time_max, 5, 1)
        layout.addWidget(self.time_binning, 6 , 1)

