""" Utility to expose all modules under src/ in the unittest directories."""

"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""
import os, sys
import os.path

file_path = os.path.dirname(__file__)
separator = os.path.sep
separated_file_path = file_path.split(separator)
top_level_index = separated_file_path.index('hedda')
separated_top_level_path = separated_file_path[:top_level_index+1]
separated_lib_level_path = separated_file_path[:top_level_index+1]
separated_top_level_path.append('src')
separated_lib_level_path.append('lib')

top_level_path = separator.join(separated_top_level_path)
lib_level_path = separator.join(separated_lib_level_path)

if not top_level_path in sys.path:
        sys.path.insert(1, top_level_path)
if not lib_level_path in sys.path:
        sys.path.insert(1, lib_level_path)


del top_level_path, lib_level_path, separated_lib_level_path, file_path, separated_file_path, separated_top_level_path

