"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys
import paths

import numpy

# Import third party libs.
import pyqtgraph as pg
from pyqtgraph.Qt import  QtGui


class RunStatisticsWidget(QtGui.QWidget):

    def __init__(self, files=None):

        self.files = files
        # Init base class.
        super( RunStatisticsWidget, self).__init__()

        ####################################
        #   RunStatistics widget           #
        ####################################
        layout = QtGui.QGridLayout()
        self.setLayout(layout)

        self.ccd1_com_plot = pg.PlotWidget(name='ccd1_com_plot', title='CCD1 COM')
        self.ccd1_wid_plot = pg.PlotWidget(name='ccd1_wid_plot', title='CCD1 Width')
        self.ccd1_int_plot = pg.PlotWidget(name='ccd1_int_plot', title='CCD1 Intensity')
        self.ccd2_com_plot = pg.PlotWidget(name='ccd2_com_plot', title='CCD2 COM')
        self.ccd2_wid_plot = pg.PlotWidget(name='ccd2_wid_plot', title='CCD2 Width')
        self.ccd2_int_plot = pg.PlotWidget(name='ccd2_int_plot', title='CCD2 Intensity')
        self.diode_array_plot = pg.PlotWidget(name='diode_array_plot', title='Diode Array')

        layout.addWidget(self.ccd1_com_plot, 0,0)
        layout.addWidget(self.ccd1_wid_plot, 0,1)
        layout.addWidget(self.ccd1_int_plot, 0,2)
        layout.addWidget(self.ccd2_com_plot, 1,0)
        layout.addWidget(self.ccd2_wid_plot, 1,1)
        layout.addWidget(self.ccd2_int_plot, 1,2)
        layout.addWidget(self.diode_array_plot, 2, 0, 1, 3)

        self.update_ccd1()
        self.update_ccd2()
        self.updateDiodeArray()


    def update_ccd1(self):
        """ Update all plots."""

        data = numpy.loadtxt(self.files[0])

        run = data[:,0]
        pos_x = data[:,1]
        pos_y = data[:,2]
        siz_x = data[:,3]
        siz_y = data[:,4]

        com_x = data[:,5]
        dcom_x = data[:,6]
        com_y = data[:,7]
        dcom_y = data[:,8]

        wid_x = data[:,9]
        dwid_x = data[:,10]
        wid_y = data[:,11]
        dwid_y = data[:,12]

        intens = data[:,13]
        dintens = data[:,14]

        com_x_err =  pg.ErrorBarItem(x=run, y=com_x, top=dcom_x, bottom=dcom_x, beam=0.1)
        com_y_err =  pg.ErrorBarItem(x=run, y=com_y, top=dcom_y, bottom=dcom_y, beam=0.1)

        wid_x_err =  pg.ErrorBarItem(x=run, y=wid_x, top=dwid_x, bottom=dwid_x, beam=0.1)
        wid_y_err =  pg.ErrorBarItem(x=run, y=wid_y, top=dwid_y, bottom=dwid_y, beam=0.1)

        intens_err =  pg.ErrorBarItem(x=run, y=intens, top=dintens, bottom=dintens, beam=0.1)

        self.ccd1_com_plot.addLegend()
        self.ccd1_com_plot.addItem( pg.PlotDataItem(run, com_x, name='  COM x', symbol='o', pen={'color':'k', 'width':0.5}, symbolBrush='r') )
        self.ccd1_com_plot.addItem( com_x_err)

        self.ccd1_com_plot.addItem( pg.PlotDataItem(run, com_y, name='  COM y', symbol='o', pen={'color':'k', 'width':0.5}, symbolBrush='b') )
        self.ccd1_com_plot.addItem( com_y_err)

        self.ccd1_wid_plot.addLegend()
        self.ccd1_wid_plot.addItem( pg.PlotDataItem(run, wid_x, name='  Width x', symbol='o', pen={'color':'k', 'width':0.5}, symbolBrush='r') )
        self.ccd1_wid_plot.addItem( wid_x_err)

        self.ccd1_wid_plot.addItem( pg.PlotDataItem(run, wid_y, name='  Width y', symbol='o', pen={'color':'k', 'width':0.5}, symbolBrush='b') )
        self.ccd1_wid_plot.addItem( wid_y_err)

        self.ccd1_int_plot.addLegend()
        self.ccd1_int_plot.addItem( pg.PlotDataItem(run, intens, name='  Intensity', symbol='o', pen={'color':'k', 'width':0.5}, symbolBrush='w') )
        self.ccd1_int_plot.addItem( intens_err)


    def update_ccd2(self):
        """ Update all plots."""

        data = numpy.loadtxt(self.files[1])

        run = data[:,0]

        pos_x_1 = data[:,1]
        pos_y_1 = data[:,2]
        siz_x_1 = data[:,3]
        siz_y_1 = data[:,4]

        pos_x_2 = data[:,5]
        pos_y_2 = data[:,6]
        siz_x_2 = data[:,7]
        siz_y_2 = data[:,8]

        com_x_1 = data[:,9]
        dcom_x_1 = data[:,10]
        com_y_1 = data[:,11]
        dcom_y_1 = data[:,12]

        wid_x_1 = data[:,13]
        dwid_x_1 = data[:,14]
        wid_y_1 = data[:,15]
        dwid_y_1 = data[:,16]

        intens_1 = data[:,17]
        dintens_1 = data[:,18]

        com_x_2 = data[:,19]
        dcom_x_2 = data[:,20]
        com_y_2 = data[:,21]
        dcom_y_2 = data[:,22]

        wid_x_2 = data[:,23]
        dwid_x_2 = data[:,24]
        wid_y_2 = data[:,25]
        dwid_y_2 = data[:,26]

        intens_2 = data[:,27]
        dintens_2 = data[:,28]


        com_x_1_err =  pg.ErrorBarItem(x=run, y=com_x_1, top=dcom_x_1, bottom=dcom_x_1, beam=0.1)
        com_y_1_err =  pg.ErrorBarItem(x=run, y=com_y_1, top=dcom_y_1, bottom=dcom_y_1, beam=0.1)

        wid_x_1_err =  pg.ErrorBarItem(x=run, y=wid_x_1, top=dwid_x_1, bottom=dwid_x_1, beam=0.1)
        wid_y_1_err =  pg.ErrorBarItem(x=run, y=wid_y_1, top=dwid_y_1, bottom=dwid_y_1, beam=0.1)

        #intens_1_err =  pg.ErrorBarItem(x=run, y=intens_1, top=dintens_1, bottom=dintens_1, beam=0.1)

        com_x_2_err =  pg.ErrorBarItem(x=run, y=com_x_2, top=dcom_x_2, bottom=dcom_x_2, beam=0.1)
        com_y_2_err =  pg.ErrorBarItem(x=run, y=com_y_2, top=dcom_y_2, bottom=dcom_y_2, beam=0.1)

        wid_x_2_err =  pg.ErrorBarItem(x=run, y=wid_x_2, top=dwid_x_2, bottom=dwid_x_2, beam=0.1)
        wid_y_2_err =  pg.ErrorBarItem(x=run, y=wid_y_2, top=dwid_y_2, bottom=dwid_y_2, beam=0.1)

        #intens_2_err =  pg.ErrorBarItem(x=run, y=intens_2, top=dintens_2, bottom=dintens_2, beam=0.1)

        self.ccd2_com_plot.addLegend()
        self.ccd2_com_plot.addItem( pg.PlotDataItem(run, com_x_1, name='COM x Mie', symbol='o', pen={'color':'k', 'width':0.5}, symbolBrush=pg.mkBrush('r')) )
        self.ccd2_com_plot.addItem( com_x_1_err)
        self.ccd2_com_plot.addItem( pg.PlotDataItem(run, com_x_2, name='COM x TS', symbol='s', pen={'color':'k', 'width':0.5}, symbolBrush=pg.mkBrush('r')) )
        self.ccd2_com_plot.addItem( com_x_2_err)

        self.ccd2_com_plot.addItem( pg.PlotDataItem(run, com_y_1, name='COM y Mie', symbol='o', pen={'color':'k', 'width':0.5}, symbolBrush='b') )
        self.ccd2_com_plot.addItem( com_y_1_err)
        self.ccd2_com_plot.addItem( pg.PlotDataItem(run, com_y_2, name='COM y TS', symbol='s', pen={'color':'k', 'width':0.5}, symbolBrush='b') )
        self.ccd2_com_plot.addItem( com_y_2_err)

        self.ccd2_wid_plot.addLegend()
        self.ccd2_wid_plot.addItem( pg.PlotDataItem(run, wid_x_1, name='Width x Mie', symbol='o', pen={'color':'k', 'width':0.5}, symbolBrush='r') )
        self.ccd2_wid_plot.addItem( wid_x_1_err)
        self.ccd2_wid_plot.addItem( pg.PlotDataItem(run, wid_x_2, name='Width x TS', symbol='s', pen={'color':'k', 'width':0.5}, symbolBrush='r') )
        self.ccd2_wid_plot.addItem( wid_x_2_err)

        self.ccd2_wid_plot.addItem( pg.PlotDataItem(run, wid_y_1, name='Width y Mie', symbol='o', pen={'color':'k', 'width':0.5}, symbolBrush='b') )
        self.ccd2_wid_plot.addItem( wid_y_1_err)
        self.ccd2_wid_plot.addItem( pg.PlotDataItem(run, wid_y_2, name='Width y TS', symbol='s', pen={'color':'k', 'width':0.5}, symbolBrush='b') )
        self.ccd2_wid_plot.addItem( wid_y_2_err)

        self.ccd2_int_plot.addLegend()
        self.ccd2_int_plot.addItem( pg.PlotDataItem(run, intens_2/intens_1 - 1., name='TS/Mie contrast', symbol='o', pen={'color':'k', 'width':0.5}, symbolBrush='w') )
        #self.ccd2_int_plot.addItem( intens_1_err)
        #self.ccd2_int_plot.addItem( pg.PlotDataItem(run, intens_2, name='Intensity TS', symbol='s', pen={'color':'k', 'width':0.5}, symbolBrush='w') )
        #self.ccd2_int_plot.addItem( intens_2_err)

    def updateDiodeArray(self):

        data = numpy.loadtxt(self.files[2])
        diodes = numpy.arange(16)
        run_indices = range(data.shape[0])
        number_of_runs = len(run_indices)
        data_vals = data[:,1::2]
        data_rmss = data[:,2::2]


        # Offset each run
        # Get largest difference:
        largest_difference = numpy.max([data_vals[i+1] - data_vals[i] for i in range(number_of_runs-1)])

        offset = 1.2*largest_difference
        for r in run_indices:
            data_vals[r] += offset * r

            err_item = pg.ErrorBarItem(x=diodes, y=data_vals[r], top=data_rmss[r], bottom=data_rmss[r], beam=0.3)
            self.diode_array_plot.addItem( pg.PlotDataItem(diodes, data_vals[r],  pen=(0,255,0) , symbolPen='w'))
            self.diode_array_plot.addItem( err_item )
























def main():
    app = QtGui.QApplication([])
    win = QtGui.QMainWindow()
    runstat = RunStatisticsWidget(files=['ccd1_log.txt', 'ccd2_log.txt'])
    win.setCentralWidget(runstat)
    win.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    #app.instance().exec_()
    main()
