""" Module for the ScopeFile class.

    @author : CFG
    @institution : XFEL
    @creation 20151024

    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.


"""
import paths
import numpy
import os
import platform


class ScopeFile:
    """
    Represents a scope file that encapsulates 2D data received from an oscilloscope.
    """

    def __init__(self, path=None):
        """
        Constructor for the ScopeFile.

        @param path : The path to the binary data
        @type : str
        @default : None
        """

        if platform.system() == 'Linux':
           from oct2py import octave as engine
        elif platform.system() == 'Windows':
           import matlab.engine
           engine = matlab.engine.start_matlab()

        #  Check path.
        if not os.path.isfile(path):
            raise RuntimeError("%s is not a file." % (path))
        self.__path = path
        #
        lecroy_lib_path = os.path.join(os.path.dirname(__file__), '..', 'lib', 'lecroy')

        # Add path where the lecroy binary reader is located to the matlab/octave engine.
        if platform.system() == 'Windows':
            engine.addpath(lecroy_lib_path, nargout=0)
        elif platform.system() == 'Linux':
            engine.addpath(lecroy_lib_path)
        # Load lecroy binary reader .m module.
        self.__waveform = engine.ReadLeCroyBinaryWaveform(path)

        if platform.system() == 'Windows':
            engine.quit()

    @property
    def waveform(self):
        """ Query the waveform object. """
        return self.__waveform

    @property
    def x(self):
        """ Query for the x values of the scope trace. """
        return numpy.array(self.waveform['x'])[:,0]


    @property
    def y(self):
        """ Query for the x values of the scope trace. """
        return numpy.array(self.waveform['y'])[:,0]

    @property
    def info(self):
        return self.waveform['info']

