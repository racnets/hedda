from ScopeFile import ScopeFile
import numpy
import itertools
import os
import matplotlib
from peakdetect import peakdetect
from matplotlib import pylab as pl

# Path to data.
data_root_dir = '../unittest/TestFiles/Scope'
run = 2

# Parameters.
light_peak_index = 4067
drift = 0.55
energy_max = 1000.0
energy_binning = 1.0
time_max = 5.0e-6
time_binning = 1.0e-8

# Assemble data directory.
run_dir = os.path.abspath( os.path.join( data_root_dir, 'run_%06d' % (run) ) )

# All files in the run directory.
scope_files = [ os.path.join( run_dir, fil ) for fil in os.listdir( run_dir ) ]

# Load the files and extract x and y arrays.
scope_files = [ ScopeFile( fil ) for fil in scope_files ]
xy_data = [[f.x, -f.y]  for  f in scope_files]

# Skip negative times.
# Assuming common x values for all traces.
x = xy_data[0][0]
positives = numpy.where(x>0.0)
low_pass = numpy.where(x<=time_max)
x = x[positives]
x = x[low_pass]

# Apply same filter to y data.
y = numpy.array([yy[1][positives][low_pass] for yy in xy_data])

# Subtract light peak time.
times = x - x[light_peak_index]

# Sum over all traces.
summed_waveform = numpy.sum(y, axis=0)

# Plot summed trace.
pl.subplot(221)
pl.plot(times,summed_waveform)
pl.xlabel('time (s)')
pl.ylabel('Scope signal')

# Find peaks.
peaks = [peakdetect.peakdetect(yy, x, lookahead=10, delta=0.015)[0] for yy in y]

# Extract maximum positions in the trace.
max_pos = [[m[0] for m in mp] for mp in peaks]
max_pos = list( itertools.chain.from_iterable( max_pos ) )

# Get binning.
number_of_time_bins = int(numpy.floor(time_max/time_binning))
number_of_energy_bins = int(numpy.floor(energy_max/energy_binning))

# Get time histogram.
time_hist, time_bins = numpy.histogram(max_pos, bins=number_of_time_bins, density=False)
time_bin_lefts = time_bins[:-1]

light_peak_time = x[light_peak_index]

# Convert from time to energy.
electron_mass=9.109e-31
electron_charge=1.62e-19
inverse_second_to_eV = 0.5*electron_mass/electron_charge
energies = numpy.array([inverse_second_to_eV*(drift/(m-light_peak_time))**2 for m in max_pos if m>light_peak_time])

energies = energies[numpy.where(energies < energy_max)]

# Get energy histogram.
en_hist, en_bins = numpy.histogram(energies, bins=number_of_energy_bins, density=False)
en_bin_lefts = en_bins[:-1]
en_bin_width = en_bins[1] - en_bins[0]


# Plot histograms.
pl.subplot(223)
pl.bar(en_bin_lefts, en_hist, width=en_bin_width)
pl.xlabel('energy (eV)')
pl.ylabel('Number of peaks')

pl.subplot(224)
pl.bar(time_bin_lefts, time_hist, width=times[1] - times[0])
pl.xlabel('time (s)')
pl.ylabel('Number of peaks')


# Plot
# Uncomment to autosave plots
#pl.savefig('run_%06d.pdf' % (run) )
pl.show()






