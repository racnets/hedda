"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""
import paths
import sys
import platform

# Import third party libs.
from pyqtgraph.Qt import QtGui

import Control

###################
# Find out the platform.
is_win    = platform.system() == 'Windows'
is_linux  = platform.system() == 'Linux'
is_macosx = platform.system() == 'Darwin'

# Find out if 32/64 bit.
is_32bit  = platform.architecture()[0] == '32bit'

def main():
    if not is_win:
        app = QtGui.QApplication([])

    #######################################
    ##    Central control                 #
    #######################################
    # Control windows.
    control_win = QtGui.QMainWindow()
    control_win.setWindowTitle('Control')
    # Init central control widget.
    control = Control.ControlWidget()
    control_win.setCentralWidget(control)

    control.destroyed.connect(control_win.close)


    # Show all windows.
    control_win.show()


    sys.exit(app.exec_())


# Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    main()
    #if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        #app.instance().exec_()

