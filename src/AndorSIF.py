""" Module for AndorSIF

    @author : CFG
    @institution : XFEL
    @creation 20151013

    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""
import os.path
import numpy
from andor_orig import SifFile
import paths

from Plotter import Plotter


class AndorSIF(SifFile, object):
    """

    """

    def __init__(self, path=None):
        """
        Constructor for the AndorSIF object.

        @param path : The path to the andor sif file.
        @type : string
        @default : None
        """

        # Check if file exists.
        if not os.path.isfile(path):
            raise RuntimeError("File %s not found or not a file." % (path))

        else:
            super(AndorSIF, self).__init__(path)

    def plotImage(self):
        """ Produce at plot using matplotlib. """

        plotter = Plotter(self.data)
        plotter.plotImage()
