"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""

import paths
import numpy
import platform


# Import third party libs.
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui


class CCD1ControlWidget(QtGui.QWidget):


    def __init__(self,**kwargs):

        super(CCD1ControlWidget, self).__init__()

        # Define layout.
        layout = QtGui.QGridLayout()
        self.setLayout(layout)

        # Spin boxes for ROI control.
        self.xmin_spin = pg.SpinBox(value=0.0, bounds=[0, None], step=1)
        self.xmax_spin = pg.SpinBox(value=10.0, bounds=[0, None], step=1)
        self.ymin_spin = pg.SpinBox(value=0.0, bounds=[0, None], step=1)
        self.ymax_spin = pg.SpinBox(value=10.0, bounds=[0, None], step=1)

        # Add on parent layout.
        layout.addWidget(QtGui.QLabel(u'CCD 90^o'), 0, 0)

        layout.addWidget(QtGui.QLabel('X min'), 1, 0)
        layout.addWidget(QtGui.QLabel('X max'), 1, 1)
        layout.addWidget(self.xmin_spin, 2, 0)
        layout.addWidget(self.xmax_spin, 2, 1)
        layout.addWidget(QtGui.QLabel('Y min'), 3, 0)
        layout.addWidget(QtGui.QLabel('Y max'), 3, 1)
        layout.addWidget(self.ymin_spin, 4, 0)
        layout.addWidget(self.ymax_spin, 4, 1)


        # Button to get statistics.
        self.statistics_button = QtGui.QPushButton('Update CCD1')
        layout.addWidget(self.statistics_button, 0, 2)

        # Data table for CCD 1 statistics.
        self.data_table_widget = pg.TableWidget()
        layout.addWidget(self.data_table_widget, 1, 2, 10, 3)

        table_data = numpy.array([
                ('X mean', 0.0),
                ('X mean rms', 0.0),
                ('Y mean', 0.0),
                ('Y mean rms', 0.0),
                ('X width', 0.0),
                ('X width rms', 0.0),
                ('Y width', 0.0),
                ('Y width rms', 0.0),
                ], dtype=[('Observable', object), ('Value', float)])


        self.data_table_widget.setData(table_data)

