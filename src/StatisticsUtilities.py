"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy
def statistics(data):
    """ Method to perform statistics on the data.
    @return : A dictionary of various statistical characteristics of the data.
    """

    # Filter out any negative intensities
    data = numpy.array([d for d in data if numpy.sum(d, axis=(0,1)) > 0.0])
    # Project data on x and y axis.
    x_projection = numpy.sum(data, axis=2)
    y_projection = numpy.sum(data, axis=1)

    # Get total intensity (sum over all pixels per frame).
    intensity = numpy.sum(x_projection, axis=1)

    total_intensity = numpy.sum(intensity)

    # Get the data shape.
    shp = data.shape
    t_len = shp[0]
    x_len = shp[1]
    y_len = shp[2]

    # Pixel axes.
    x_axis = numpy.arange( x_len )
    y_axis = numpy.arange( y_len )

    # Get the center of mass for each x and y projection.
    pixel_x = x_projection.dot(x_axis) / intensity
    pixel_y = y_projection.dot(y_axis) / intensity

    # Get the center of mass averaged over all frames.
    mean_pixel_x = numpy.sum( pixel_x * intensity ) / total_intensity
    mean_pixel_y = numpy.sum( pixel_y * intensity ) / total_intensity

    # Get the width of each projection.
    shifted_x_axes = numpy.array( [x_axis - px for px in pixel_x] )
    shifted_y_axes = numpy.array( [y_axis - px for px in pixel_y] )
    width_x = [numpy.sqrt(numpy.sum( x_projection[i]*(shifted_x_axes[i]**2) )  / intensity[i]) for i in range(t_len)]
    width_y = [numpy.sqrt(numpy.sum( y_projection[i]*(shifted_y_axes[i]**2) )  / intensity[i]) for i in range(t_len)]

    # Get the width averaged over all frames, weight according to frame's intensity.
    mean_width_x = numpy.sum( width_x * intensity ) / total_intensity
    mean_width_y = numpy.sum( width_y * intensity ) / total_intensity

    # Get the rms deviation of the center of masses.

    rms_pixel_x = numpy.sqrt( numpy.sum( (pixel_x - mean_pixel_x)**2 * intensity ) / total_intensity )
    rms_pixel_y = numpy.sqrt( numpy.sum( (pixel_y - mean_pixel_y)**2 * intensity ) / total_intensity )

    # Get the rms deviation of the projection widths.
    rms_width_x =  numpy.sqrt( numpy.sum( (width_x - mean_width_x)**2 * intensity ) / total_intensity )
    rms_width_y =  numpy.sqrt( numpy.sum( (width_y - mean_width_y)**2 * intensity ) / total_intensity )

    mean_intensity = numpy.mean(intensity)
    rms_intensity = numpy.std(intensity)

    stat_dict = {'mean_pixel_x'   : mean_pixel_x,
                 'mean_pixel_y'   : mean_pixel_y,
                 'mean_width_x'   : mean_width_x,
                 'mean_width_y'   : mean_width_y,
                 'rms_pixel_x'    : rms_pixel_x,
                 'rms_pixel_y'    : rms_pixel_y,
                 'rms_width_x'    : rms_width_x,
                 'rms_width_y'    : rms_width_y,
                 'mean_intensity' : mean_intensity,
                 'rms_intensity'  : rms_intensity,
                 }

    return stat_dict
