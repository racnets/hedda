import numpy
import os
from matplotlib import image, pylab

path_to_tiffs = "F:/TS_data_dec_2015/1207_02h30_run1/CCD2/tiff"

log = path_to_tiffs+'log.txt'

tiffs = [os.path.join(path_to_tiffs, tif) for tif in os.listdir(path_to_tiffs) if 'tif' in tif]
print 'Found %d tifs.' % len(tiffs)


nlo_xmin = 0
nlo_xmax = 2047
nlo_ymin = 0
nlo_ymax = 2047

blo_xmin = 0
blo_xmax = 2047
blo_ymin = 0
blo_ymax = 2047

number_of_images = len(tiffs)
indices = numpy.arange(number_of_images)
intensities = []
nlo_intensities = []
blo_intensities = []

for tif in tiffs:
    im = image.imread(tif).transpose()
    
    intensities.append(im.sum().sum())
    #nlo_intensities.append(numpy.sum(im[nlo_xmin:nlo_xmax, nlo_ymin:nlo_ymax]).sum() )
    #blo_intensities.append(numpy.sum(im[blo_xmin:blo_xmax, blo_ymin:blo_ymax]).sum() )
    
intensity_histogram, bins = numpy.histogram(intensities)   
center = (bins[:-1] + bins[1:]) / 2
pylab.figure(0)
pylab.bar(center, intensity_histogram)
#pylab.figure(1)
#pylab.plot(indices, nlo_intensities)
#pylab.figure(2)
#pylab.plot(indices, blo_intensities)

pylab.show()
>>>>>>> Adding ccd2 analysis script.
