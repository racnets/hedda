import numpy
import scipy
import matplotlib.pyplot as plt

from scipy import optimize, special, signal, ndimage
from PIL import Image


#def b_I(R, order):
#	refract=1.1
#	num = (refract*scipy.special.jv(order, R)*scipy.special.jvp(order, refract*R) - scipy.special.jv(order, refract*R)*scipy.special.jvp(order, R))
#	denom = (refract*scipy.special.hankel1(order, R)*scipy.special.jvp(order, refract*R) - scipy.special.jv(order, refract*R)*scipy.special.h1vp(order, R))
#	return -numpy.abs(num/denom)

im = Image.open(r'F:\TS_data_dec_2015\1207_03h07_run1\CCD2_tiff_rot\CCD2_run-10.tif')
w, h = im.size
im_cropped = im.crop((300, 300, w-300, h-300))

blurred_im = ndimage.gaussian_filter(im_cropped, 3)
filter_blurred_im = ndimage.gaussian_filter(blurred_im, 1)
alpha = 100
sharpened = blurred_im + alpha * (blurred_im - filter_blurred_im)

#Image.fromarray(sharpened).show()

imarray = ()
imarray = numpy.array(sharpened)

imarray_fft = numpy.abs(numpy.fft.fft(imarray, axis=-1))
imarray_lineout = numpy.sum(imarray_fft, axis=-2)

freq = numpy.fft.fftfreq(len(imarray), 13.5e-6)
ang_freq = 2*numpy.pi*freq*0.280

pos_ang_freq = (numpy.array_split(ang_freq, 2))[0]
pos_lineout = (numpy.array_split(imarray_lineout, 2))[0]

#integer_lineout = numpy.interp(numpy.arange(0,1e4,0.01), pos_ang_freq, pos_lineout)

#radii = numpy.array

#for i in numpy.arange(0,1e4,10):
#	radii = numpy.apend(radii, scipy.optimize.minimize(b_I, 1e-5, args=(i)).x)

x_values = pos_ang_freq*13.5e-9*1e6/(2*numpy.pi*numpy.cos(numpy.pi*21.0/180))
final_lineout = numpy.column_stack((x_values, pos_lineout))

#peaks = scipy.signal.find_peaks_cwt(pos_lineout, numpy.arange(1.0,3.0,0.2))

plt.plot(x_values, pos_lineout)
plt.axis([0,14,0,1e8])
plt.xlabel('Diameter [10^-6 m]')
plt.show()
#numpy.savetxt(r'F:\TS_data_dec_2015\1207_02h30_run1\CCD2_tiff_rot\lineout-1.txt', final_lineout, fmt='%.5e')


