""" Module for a Plotter based on matplotlib.

    @author : CFG
    @institution : XFEL
    @creation 20151013

    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""

import matplotlib
#matplotlib.use("pdf")
from matplotlib import pyplot

import numpy



class Plotter(object):
    """
    Class that represents plotting capabilities for given input data.
    """

    def __init__(self, data=None):
        """
        Constructor for the Plotter.

        @param data : The data to plot.
        @type : numpy array. Various shapes are accepted.
        """

        ###TODO: check data for shape.

        self.__data = [data]

    def setData(self, data):
        """ Setter for the data array. """
        self.__data = [data]

    def plotImage(self):
        """ Plot the image using matplotlib. """

        self.imgplot = pyplot.imshow(self.__data[0], cmap=pyplot.cm.RdBu)
        pyplot.colorbar(self.imgplot)

        pyplot.show()

    def plotXY(self):
        """ Produces a x-y graph from given data. """
        for data_set in self.__data:
            if len(data_set) == 1:
                pyplot.plot(data_set[0])
            else:
                [pyplot.plot(data_set[0], data_set[i+1]) for i in range(len(data_set)-1)]

        pyplot.show()


    def addData(self, data):
        """ Add another data set to already existing. """
        if len(self.__data[0]) <= 2:
            self.__data.append(data)
        else:
            raise RuntimeError("Adding a new data set is only possible if the existing data is 2D (x-y data).")

