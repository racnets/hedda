""" Module for ROI

    @author : CFG
    @institution : XFEL
    @creation 20151016

    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""


import numpy


class ROI():
    """
    Class for representation of a region of interest (ROI).
    """

    def __init__(self, frame_min=None, frame_max=None, x_min=None, x_max=None, y_min=None, y_max=None):
        """
        Constructor for the ROI.

        @param frame_min : Minimum frame to take into account.
        @type : int
        @default : 0

        @param frame_max : Maximum frame to take into account.
        @type : int
        @default : None

        @param y_min : First pixle number in the x direction.
        @type : int
        @default : 0

        @param y_max : Last pixle number in the x direction.
        @type : int
        @default : None

        @param y_min : First pixle number in the y direction.
        @type : int
        @default : 0

        @param y_max : Last pixle number in the y direction.
        @type : int
        @default : None
        """
        if any([a is None for a in [frame_min, frame_max, x_min, x_max, y_min, y_max]]):
                raise RuntimeError('Min/Max parameters cannot be None.')

        self.__frame_min, self.__frame_max = checkAndSetMinMax(frame_min, frame_max, "frame")
        self.__x_min, self.__x_max = checkAndSetMinMax(x_min, x_max, "x")
        self.__y_min, self.__y_max = checkAndSetMinMax(y_min, y_max, "y")

    @property
    def x_min(self):
        return self.__x_min

    @x_min.setter
    def x_min(self, x_min):
        self.__x_min, self.__x_max = checkAndSetMinMax(x_min, self.__x_max, 'x')

    @property
    def x_max(self):
        return self.__x_max

    @x_max.setter
    def x_max(self, x_max):
        self.__x_min, self.__x_max = checkAndSetMinMax(self.__x_min, x_max, 'x')

    def setXMinMax(self, x_min, x_max):
        self.__x_min, self.__x_max = checkAndSetMinMax(x_min, x_max, 'x')

    @property
    def y_min(self):
        return self.__y_min

    @y_min.setter
    def y_min(self, y_min):
        self.__y_min, self.__y_max = checkAndSetMinMax(y_min, self.__y_max, 'y')

    @property
    def y_max(self):
        return self.__y_max

    @y_max.setter
    def y_max(self, y_max):
        self.__y_min, self.__y_max = checkAndSetMinMax(self.__y_min, y_max, 'y')

    def setYMinMax(self, y_min, y_max):
        self.__y_min, self.__y_max = checkAndSetMinMax(y_min, y_max)

    @property
    def frame_min(self):
        return self.__frame_min

    @frame_min.setter
    def frame_min(self, frame_min):
        self.__frame_min, self.__frame_max = checkAndSetMinMax(frame_min, self.__frame_max, 'frame')

    @property
    def frame_max(self):
        return self.__frame_max

    @frame_max.setter
    def frame_max(self, frame_max):
        self.__frame_min, self.__frame_max = checkAndSetMinMax(self.__frame_min, frame_max, 'frame')

    def setFrameMinMax(self, frame_min, frame_max):
        self.__frame_min, self.__frame_max = checkAndSetMinMax(frame_min, frame_max, 'frame')


def checkAndSetMinMax(var_min=None, var_max=None, var_name=None):
            """ Utility to check if minimum and maximum are sensible. """

            try:
                var_max = int(var_max)
                var_min = int(var_min)
            except:
                raise TypeError("Maximum and minimum specifiers for %s must be an integer." % (var_name))

            if var_max <= var_min:
                raise TypeError("Maximum specifier %s_max must be > minimum specifier %s_min." % (var_name, var_name))

            return var_min, var_max


