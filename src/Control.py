"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy
import os, sys
import paths
import matplotlib

# Import third party libs.
from pyqtgraph.Qt import QtGui, QtCore

from RunStatistics import RunStatisticsWidget

from CCD1ControlWidget import CCD1ControlWidget
import DiodeArray
import CCD
import CCD_Contrast
import Scope
#import Scope_cwt as Scope
from CCD2ControlWidget import CCD2ControlWidget
from ScopeControlWidget import ScopeControlWidget
from InputPathsWidget import InputPathsWidget
from RunNumberWidget import RunNumberWidget

from HedSPE import HedSPE
from AndorSIF import AndorSIF
from ScopeFile import ScopeFile
from DiodeArrayFile import DiodeArrayFile


class ControlWidget(QtGui.QWidget):
        # Central control widget. Owns all diagnostic widgets.

    def __init__(self):

        # Init base class.
        super(ControlWidget, self).__init__()

        # Define layout.
        control_layout = QtGui.QGridLayout()
        self.setLayout(control_layout)

        # Input paths.
        self.input_paths_widget = InputPathsWidget()
        control_layout.addWidget(self.input_paths_widget, 0, 0)

        # Run Nr.
        self.run_number_widget = RunNumberWidget()
        control_layout.addWidget(self.run_number_widget, 0, 1)

        self.run_number_widget.update_widget.clicked.connect(self.updateAll)
        self.run_number_widget.close_button.clicked.connect(self.close)

        # Plot all runs.
        self.run_number_widget.plot_runs_button.clicked.connect(self.plotAllRuns)

        # CCD 1 control widget.
        self.ccd1_control = CCD1ControlWidget()
        control_layout.addWidget(self.ccd1_control, 4, 0)
        self.ccd1 = CCD.CCDWidget(data=None)
        self.ccd1_win = QtGui.QMainWindow()
        self.ccd1_win.setWindowTitle("CCD 1 (90^o)")
        self.ccd1_win.setCentralWidget(self.ccd1)

        # CCD 2 control widget.
        self.ccd2_control = CCD2ControlWidget()
        control_layout.addWidget(self.ccd2_control, 5, 0)
        self.ccd2 = CCD_Contrast.CCDWidget(data=None)
        self.ccd2_win = QtGui.QMainWindow()
        self.ccd2_win.setCentralWidget(self.ccd2)
        self.ccd2_win.setWindowTitle("CCD 2 (40^o)")

        # Scope control #
        self.scope_control = ScopeControlWidget()
        control_layout.addWidget(self.scope_control, 4, 1)
        self.scope = Scope.ScopeWidget(scope_files=None)
        self.scope_win = QtGui.QMainWindow()
        self.scope_win.setWindowTitle("Scope")
        self.scope_win.setCentralWidget(self.scope)

        self.diode_array = DiodeArray.DiodeArrayWidget()
        self.diode_win = QtGui.QMainWindow()
        self.diode_win.setWindowTitle("Diode Arrays (interleaved)")
        self.diode_win.setCentralWidget(self.diode_array)


        # Set connected flag to false. This effectively blocks all signals that are emitted upon changing any
        # analysis parameters.
        self.__is_connected = False

    def setupLogFiles(self):
        """
        Checks if output files exists and opens them in append mode.
        If file is new, writes a header line.
        """

        # Assemble log directory path.
        log_dir = os.path.join( str( self.input_paths_widget.root_input_dir.text() ) ,
                                 str( self.input_paths_widget.log_input_dir.text() ) )

        self.log_dir = log_dir

        # Create log directory if not existing.
        if not os.path.isdir(log_dir):
            os.mkdir(log_dir)

        # Assemble paths for instrument logs.
        self.ccd1_outfilename = os.path.join(log_dir, 'ccd1_log.txt')
        self.ccd2_outfilename = os.path.join(log_dir, 'ccd2_log.txt')
        self.diode_array_outfilename = os.path.join(log_dir, 'diode_array_log.txt')

        # Write header if files are new.
        if not os.path.isfile(self.ccd1_outfilename):
            with open(self.ccd1_outfilename, 'w') as self.ccd1_outfile:
                self.ccd1_outfile.write('# Run number   \t\
x pos      \t\
y pos      \t\
x siz      \t\
y siz      \t\
x COM mean \t\
x COM rms  \t\
y COM mean \t\
y COM rms  \t\
x width mean \t\
x width rms  \t\
y width mean \t\
y width rms  \t\
mean intens  \t\
rms  intens  \n')

        if not os.path.isfile(self.ccd2_outfilename):
            with open(self.ccd2_outfilename, 'w') as self.ccd2_outfile:
                self.ccd2_outfile.write('# Run number \t\
x1 pos      \t\
y1 pos      \t\
x1 siz      \t\
y1 siz      \t\
x2 pos      \t\
y2 pos      \t\
x2 siz      \t\
y2 siz      \t\
x1 COM mean \t\
x1 COM rms  \t\
y1 COM mean \t\
y1 COM rms  \t\
x1 width mean \t\
x1 width rms  \t\
y1 width mean \t\
y1 width rms  \t\
mean intens1  \t\
rms  intens1  \t\
x2 COM mean \t\
x2 COM rms  \t\
y2 COM mean \t\
y2 COM rms  \t\
x2 width mean \t\
x2 width rms  \t\
y2 width mean \t\
y2 width rms  \t\
mean intens2  \t\
rms  intens2  \n'
)

        #if not os.path.isfile('scope_log.txt'):
            #self.scope_outfile = open('scope_log.txt', 'w')

        if not os.path.isfile('diode_array_log.txt'):
            with open('diode_array_log.txt', 'w') as self.diode_array_outfile:
                self.diode_array_outfile.write('# Run number \t\
D0 mean      \t\
D0 rms       \t\
D1 mean      \t\
D1 rms       \t\
D2 mean      \t\
D2 rms       \t\
D3 mean      \t\
D3 rms       \t\
D4 mean      \t\
D4 rms       \t\
D5 mean      \t\
D5 rms       \t\
D6 mean      \t\
D6 rms       \t\
D7 mean      \t\
D7 rms       \t\
D8 mean      \t\
D8 mean      \t\
D9 rms       \t\
D9 rms       \t\
D10 mean      \t\
D10 rms       \t\
D11 mean      \t\
D11 rms       \t\
D12 mean      \t\
D12 rms       \t\
D13 mean      \t\
D13 rms       \t\
D14 mean      \t\
D14 rms       \t\
D15 mean      \t\
D15 rms       \n')

    def updateAll(self):
        # Update run number.
        self.run_number = self.run_number_widget.run_number.value()
        self.run_string ='run-%d' % (self.run_number)

        # Setup the log files.
        self.setupLogFiles()

        # Connect all signals.
        self.makeConnections()

        # Update all plots.
        #self.updateCCD1()
        self.updateCCD2()
        self.updateDiodeArray()

        #self.updateScope()
        # Updating the scope widget takes long. Therefore we thread it out.
        #self.scope_update_thread = ScopeUpdateThread(parent=self)
        #self.scope_update_thread.start()

        #self.scope_win.show()

    def makeConnections(self):

        # If already connected, just return.
        if self.__is_connected:
            return

        # Connect button signal to action.
        self.ccd1_control.statistics_button.clicked.connect(self.ccd1StatButtonPushed)
        self.ccd2_control.statistics_button.clicked.connect(self.ccd2StatButtonPushed)
        self.run_number_widget.save_button.clicked.connect(self.saveButtonPushed)

        #self.ccd1_control.xmin_spin.sigValueChanged.connect(self.updateCCD1ROI)
        #self.ccd1_control.xmax_spin.sigValueChanged.connect(self.updateCCD1ROI)
        #self.ccd1_control.ymin_spin.sigValueChanged.connect(self.updateCCD1ROI)
        #self.ccd1_control.ymax_spin.sigValueChanged.connect(self.updateCCD1ROI)

        #self.ccd2_control.xmin_spin.sigValueChanged.connect(self.updateCCD2ROI)
        #self.ccd2_control.xmax_spin.sigValueChanged.connect(self.updateCCD2ROI)
        #self.ccd2_control.ymin_spin.sigValueChanged.connect(self.updateCCD2ROI)
        #self.ccd2_control.ymax_spin.sigValueChanged.connect(self.updateCCD2ROI)

        #self.ccd2_control.xmin_spin_2.sigValueChanged.connect(self.updateCCD2ROI2)
        #self.ccd2_control.xmax_spin_2.sigValueChanged.connect(self.updateCCD2ROI2)
        #self.ccd2_control.ymin_spin_2.sigValueChanged.connect(self.updateCCD2ROI2)
        #self.ccd2_control.ymax_spin_2.sigValueChanged.connect(self.updateCCD2ROI2)

        self.ccd1.frames_view.roi.sigRegionChanged.connect(self.updateCCD1Control)
        self.ccd2.frames_view.roi.sigRegionChanged.connect(self.updateCCD2Control)
        self.ccd2.bg_roi.sigRegionChanged.connect(self.updateCCD2Control2)

        self.scope_control.light_peak_index.sigValueChanged.connect(self.scope.lightPeakIndexChanged)
        self.scope_control.drift.sigValueChanged.connect(self.scope.driftChanged)
        self.scope_control.energy_max.sigValueChanged.connect(self.scope.energyMaxChanged)
        self.scope_control.energy_binning.sigValueChanged.connect(self.scope.energyBinningChanged)
        self.scope_control.time_max.sigValueChanged.connect(self.scope.timeMaxChanged)
        self.scope_control.time_binning.sigValueChanged.connect(self.scope.timeBinningChanged)

        # Set connected flag to true, so we don't have to do this again.
        self.__is_connected = True


    def closeEvent(self, event):
        # Overloading the closeEvent, so we can close all analysis widgets in one go.

        if hasattr(self, 'ccd1'):
            self.ccd1.close()
            del self.ccd1
        if hasattr(self, 'ccd1_win'):
            del self.ccd1_win

        if hasattr(self, 'ccd2'):
            self.ccd2.close()
            del self.ccd2
        if hasattr(self, 'ccd2_win'):
            del self.ccd2_win

        if hasattr(self, 'scope'):
            self.scope.close()
            del self.scope
        if hasattr(self, 'scope_win'):
            del self.scope_win

        if hasattr(self, 'diode_array'):
            self.diode_array.close()
            del self.diode_array
        if hasattr(self, 'diode_win'):
            del self.diode_win

        # Close control widget and exit the application.
        event.accept()
        sys.exit()


    def updateCCD1(self):

        ccd1_dir = os.path.join( str( self.input_paths_widget.root_input_dir.text() ) ,
                                 str( self.input_paths_widget.ccd1_input_dir.text() ) )

        ccd1_file = os.path.join(ccd1_dir, 'CCD1_' + self.run_string + '.SPE')
        #print ccd1_file
        spe = HedSPE(ccd1_file, background_frame=None, background_data=None)

        ccd1_data = numpy.array([d.transpose() for d in spe.data])
        self.ccd1.setData(data=ccd1_data)
        self.ccd1.update()
        self.updateCCD1ROI()
        self.ccd1.updateLineout()

        self.ccd1_win.show()

    def updateCCD2(self):

        ccd2_dir = os.path.join( str( self.input_paths_widget.root_input_dir.text() ) ,
                                 str( self.input_paths_widget.ccd2_input_dir.text() ) )

        #ccd2_file = os.path.join(ccd2_dir, 'CCD2_' + self.run_string + '.SPE')
        #ccd2_file = os.path.join(ccd2_dir, 'CCD2_' + self.run_string + '.sif')
        ccd2_file = os.path.join(ccd2_dir, 'tiff\CCD2_' + self.run_string + '.tif')
        #spe2 = HedSPE(ccd2_file, background_frame=None, background_data=None)
        #spe2 = AndorSIF(ccd2_file)
        ccd2_data = matplotlib.image.imread(ccd2_file)
        #ccd2_data = numpy.array([d.transpose() for d in [ccd2_data]])

        # Remove last column and row
        #ccd2_data = ccd2_data[1:-1, 1:-1]

        self.ccd2.setData(ccd2_data)
        self.ccd2.update()
        self.updateCCD2ROI()
        self.updateCCD2ROI2()
        self.ccd2.updateLineout()

        self.ccd2_win.show()

    def updateScopeFiles(self):

        scope_dir = os.path.join( str( self.input_paths_widget.root_input_dir.text() ) ,
                                  str( self.input_paths_widget.scope_input_dir.text() ) )
        scope_run_dir = os.path.join(scope_dir, self.run_string)

        scope_files = [os.path.join(scope_run_dir, sf) for sf in os.listdir(scope_run_dir) if sf.split('.')[-1] == 'trc' ]
        scope_files = [ScopeFile(sf) for sf in scope_files ]

        self.scope.setFiles(scope_files)

    def updateScope(self):
        self.scope.update()


    def updateDiodeArray(self):

        diode_array_dir = os.path.join( str( self.input_paths_widget.root_input_dir.text() ) ,
                                        str( self.input_paths_widget.diode_array_input_dir.text() ) )
        diode_array_file = DiodeArrayFile( os.path.join(diode_array_dir, 'diode_array_'+self.run_string+'.dat') )
        self.diode_array.setFile(diode_array_file)
        self.diode_array.update()

        self.diode_win.show()

    def updateCCD1ROI(self):
        xmin = self.ccd1_control.xmin_spin.value()
        xmax = self.ccd1_control.xmax_spin.value()
        ymin = self.ccd1_control.ymin_spin.value()
        ymax = self.ccd1_control.ymax_spin.value()

        self.ccd1.frames_view.roi.setPos(pos=(xmin, ymin))
        self.ccd1.frames_view.roi.setSize(size=(xmax-xmin, ymax-ymin))
        self.ccd1.frames_view.roi.show()

    def updateCCD2ROI(self):
        xmin = self.ccd2_control.xmin_spin.value()
        xmax = self.ccd2_control.xmax_spin.value()
        ymin = self.ccd2_control.ymin_spin.value()
        ymax = self.ccd2_control.ymax_spin.value()

        self.ccd2.frames_view.roi.setPos(pos=(xmin, ymin))
        self.ccd2.frames_view.roi.setSize(size=(xmax-xmin, ymax-ymin))
        self.ccd2.frames_view.roi.show()

    def updateCCD2ROI2(self):
        xmin = self.ccd2_control.xmin_spin_2.value()
        xmax = self.ccd2_control.xmax_spin_2.value()
        #ymin = self.ccd2_control.ymin_spin_2.value()
        #ymax = self.ccd2_control.ymax_spin_2.value()
        ymin = self.ccd2_control.ymin_spin.value()
        ymax = self.ccd2_control.ymax_spin.value()

        self.ccd2.bg_roi.setPos(pos=(xmin, ymin))
        self.ccd2.bg_roi.setSize(size=(xmax-xmin, ymax-ymin))
        self.ccd2.bg_roi.show()


    def updateCCD1Control(self):
        xmin, ymin = self.ccd1.frames_view.roi.pos()
        dx, dy = self.ccd1.frames_view.roi.size()
        xmax, ymax = xmin+dx, ymin+dy

        self.ccd1_control.xmin_spin.setValue(xmin)
        self.ccd1_control.ymin_spin.setValue(ymin)
        self.ccd1_control.xmax_spin.setValue(xmax)
        self.ccd1_control.ymax_spin.setValue(ymax)

    def updateCCD2Control(self):
        xmin, ymin = self.ccd2.frames_view.roi.pos()
        dx, dy = self.ccd2.frames_view.roi.size()
        xmax, ymax = xmin+dx, ymin+dy

        self.ccd2_control.xmin_spin.setValue(xmin)
        self.ccd2_control.ymin_spin.setValue(ymin)
        self.ccd2_control.xmax_spin.setValue(xmax)
        self.ccd2_control.ymax_spin.setValue(ymax)

    def updateCCD2Control2(self):
        xmin = self.ccd2.bg_roi.pos()[0]
        ymin = self.ccd2.frame_roi.pos()[1]
        dx = self.ccd2.bg_roi.size()[0]
        dy = self.ccd2.frame_roi.size()[1]
        xmax, ymax = xmin+dx, ymin+dy

        self.ccd2_control.xmin_spin_2.setValue(xmin)
        self.ccd2_control.ymin_spin_2.setValue(ymin)
        self.ccd2_control.xmax_spin_2.setValue(xmax)
        self.ccd2_control.ymax_spin_2.setValue(ymax)

    # Button pushed action.
    def ccd1StatButtonPushed(self):
        """ Defines what happens if the button is pushed """

        self.ccd1.updateLineout()
        self.ccdStatButtonPushed(self.ccd1, self.ccd1_control.data_table_widget)

    def ccd2StatButtonPushed(self):
        """ Defines what happens if the button is pushed """

        self.ccd2.updateLineout()

        self.ccdStatButtonPushed(self.ccd2, self.ccd2_control.data_table_widget_1, self.ccd2.frame_roi)
        self.ccdStatButtonPushed(self.ccd2, self.ccd2_control.data_table_widget_2, self.ccd2.bg_roi)
        # New window to print data.


    # Save button clicked action.
    def saveButtonPushed(self):
        """ Action upon click on save button. """


        # Get statistics from CCD 1.
        ccd1_stat = self.ccd1.getStatistics()

        table_data = [
             ccd1_stat['mean_pixel_x'],
             ccd1_stat['rms_pixel_x'],
             ccd1_stat['mean_pixel_y'],
             ccd1_stat['rms_pixel_y'],
             ccd1_stat['mean_width_x'],
             ccd1_stat['rms_width_x'],
             ccd1_stat['mean_width_y'],
             ccd1_stat['rms_width_y'],
             ccd1_stat['mean_intensity'],
             ccd1_stat['rms_intensity'],
             ]

        # Get ROI position and size.
        roi_pos = self.ccd1.frames_view.roi.pos()
        roi_size = self.ccd1.frames_view.roi.size()

        # Aggregate the line to print.
        out_str = str(self.run_number) + '\t' +  '\t'.join(['%6.4f' % (t) for t in roi_pos ]) \
                                       + '\t' +  '\t'.join(['%6.4f' % (t) for t in roi_size ]) \
                                       + '\t' + '\t'.join(['%6.4f' % (t) for t in table_data])

        # Write to log.
        with open(self.ccd1_outfilename, 'a') as self.ccd1_outfile:
            self.ccd1_outfile.write(out_str+'\n')

        # Get Statistics for CCD 2.
        ccd2_stat_1 = self.ccd2.getStatistics()
        ccd2_stat_2 = self.ccd2.getStatistics(self.ccd2.bg_roi)

        table_data = [
             ccd2_stat_1['mean_pixel_x'],
             ccd2_stat_1['rms_pixel_x'],
             ccd2_stat_1['mean_pixel_y'],
             ccd2_stat_1['rms_pixel_y'],
             ccd2_stat_1['mean_width_x'],
             ccd2_stat_1['rms_width_x'],
             ccd2_stat_1['mean_width_y'],
             ccd2_stat_1['rms_width_y'],
             ccd2_stat_1['mean_intensity'],
             ccd2_stat_1['rms_intensity'],
             ccd2_stat_2['mean_pixel_x'],
             ccd2_stat_2['rms_pixel_x'],
             ccd2_stat_2['mean_pixel_y'],
             ccd2_stat_2['rms_pixel_y'],
             ccd2_stat_2['mean_width_x'],
             ccd2_stat_2['rms_width_x'],
             ccd2_stat_2['mean_width_y'],
             ccd2_stat_2['rms_width_y'],
             ccd2_stat_2['mean_intensity'],
             ccd2_stat_2['rms_intensity'],
             ]

        # Get ROI positions and sizes.
        roi_pos = self.ccd2.frame_roi.pos()
        roi_size = self.ccd2.frame_roi.size()
        bg_roi_pos = self.ccd2.bg_roi.pos()
        bg_roi_size = self.ccd2.bg_roi.size()

        # Aggregate line to print.
        out_str = str(self.run_number) + \
                '\t' + '\t'.join(['%6.4f' % (t) for t in roi_pos]) + \
                '\t' + '\t'.join(['%6.4f' % (t) for t in roi_size]) + \
                '\t' + '\t'.join(['%6.4f' % (t) for t in bg_roi_pos]) + \
                '\t' + '\t'.join(['%6.4f' % (t) for t in bg_roi_size]) + \
                '\t' + '\t'.join(['%6.4f' % (t) for t in table_data])

        # Write to log.
        with open(self.ccd2_outfilename, 'a') as self.ccd2_outfile:
            self.ccd2_outfile.write(out_str+'\n')

        # Get Diode Array data.
        da_data = numpy.array( [ [ mean, rms ] for (mean, rms) in zip(self.diode_array.diode_array_mean, self.diode_array.diode_array_rms)]).flatten()

        # Aggregate line to print.
        out_str = str(self.run_number) + '\t' + '\t'.join(['%6.4f' % (t) for t in da_data])

        # Write to log.
        with open(self.diode_array_outfilename, 'a') as self.diode_array_outfile:
            self.diode_array_outfile.write(out_str + '\n')


    def ccdStatButtonPushed(self, ccd_widget, table_widget, roi=None):

        stat= ccd_widget.getStatistics(roi)

        table_data = numpy.array([
            ('X mean', stat['mean_pixel_x']),
            ('X mean rms', stat['rms_pixel_x']),
            ('Y mean', stat['mean_pixel_y']),
            ('Y mean rms', stat['rms_pixel_y']),
            ('X width', stat['mean_width_x']),
            ('X width rms', stat['rms_width_x']),
            ('Y width', stat['mean_width_y']),
            ('Y width rms', stat['rms_width_y']),
            ], dtype=[('Observable', object), ('Value', float)])

        table_widget.setData(table_data)


    def plotAllRuns(self):
        """
        Opens a window with plots for the statistical characteristics as function of run number.
        """
        self.runstat_win = QtGui.QMainWindow()
        self.runstat = RunStatisticsWidget(files=[self.ccd1_outfilename, self.ccd2_outfilename, self.diode_array_outfilename])
        self.runstat_win.setCentralWidget(self.runstat)
        self.runstat_win.show()

class ScopeUpdateThread(QtCore.QThread):
    def __init__(self, parent):
        super(ScopeUpdateThread, self).__init__(parent)

        self.finished.connect(parent.updateScope)

    def run(self):
        self.parent().updateScopeFiles()

