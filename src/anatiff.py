import matplotlib
matplotlib.use(u'Qt4Agg')
from matplotlib import pylab

import os
import numpy

path_to_tiffs = 'F:/TS_data_dec_2015/aquivalent plane monitor'

okzs = [167,
        177,
        187,
        197,
        217,
        227]
        
tiffs = [os.path.join(path_to_tiffs, tiff) for tiff in os.listdir(path_to_tiffs) if 'FEL_z' in tiff]

zs = []


x_com = []
y_com = []
x_wid = []
y_wid = []
    
 
for tiff in tiffs:
    z = int(tiff.split('FEL_z')[1].split('_')[0])
    if z in okzs:
        try:
            im = pylab.imread(tiff)                                              
            x_lineout = numpy.sum(im, axis=0)
            y_lineout = numpy.sum(im, axis=1)
            x_axis = numpy.arange(im.shape[1])
            y_axis = numpy.arange(im.shape[0])
            x_intensity = numpy.sum(x_lineout)
            y_intensity = numpy.sum(y_lineout)
            
            x = numpy.sum(x_axis * x_lineout) / x_intensity
            x_com.append(x)
            y = numpy.sum(y_axis * y_lineout) / y_intensity
            y_com.append(y)
            #
            x_wid.append((numpy.sum((x_axis - x)**2 * x_lineout))**0.5 / x_intensity)
            y_wid.append((numpy.sum((y_axis - y)**2 * y_lineout))**0.5 / y_intensity)
            #
            zs.append(int(tiff.split('FEL_z')[1].split('_')[0]))
            
        except:
            print tiff + "failed." 
            
unique_zs = list(set(zs))
unique_zs.sort()    
indices_for_z = [unique_zs.index(z) for z in zs]
    
number_of_summands = numpy.zeros(len(unique_zs))
x_com_uniques = numpy.zeros(len(unique_zs)) 
y_com_uniques = numpy.zeros(len(unique_zs))
x_wid_uniques = numpy.zeros(len(unique_zs)) 
y_wid_uniques = numpy.zeros(len(unique_zs))
for i in range(len(x_com)):
    x_com_uniques[indices_for_z[i]] += x_com[i]  
    y_com_uniques[indices_for_z[i]] += y_com[i]
    x_wid_uniques[indices_for_z[i]] += x_wid[i]  
    y_wid_uniques[indices_for_z[i]] += y_wid[i]
    number_of_summands[indices_for_z[i]] += 1

x_com_uniques = x_com_uniques / number_of_summands
y_com_uniques = y_com_uniques / number_of_summands
x_wid_uniques = x_wid_uniques / number_of_summands
y_wid_uniques = y_wid_uniques / number_of_summands


print x_com_uniques
print y_com_uniques
print x_wid_uniques
print y_wid_uniques

pylab.figure(0)
pylab.plot(unique_zs, x_com_uniques)
pylab.plot(unique_zs, y_com_uniques)
pylab.figure(1)
pylab.plot(unique_zs, x_wid_uniques)
pylab.plot(unique_zs, y_wid_uniques)
pylab.show()