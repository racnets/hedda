"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""


import paths
import numpy
import platform


# Import third party libs.
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui


class RunNumberWidget(QtGui.QWidget):


    def __init__(self,**kwargs):

        super(RunNumberWidget, self).__init__()

        # Define layout.
        layout = QtGui.QGridLayout()
        self.setLayout(layout)

        run_number_label = QtGui.QLabel("Run")
        self.run_number = pg.SpinBox(value=1, bounds=[0, None], step=1)

        layout.addWidget(run_number_label, 0, 0)
        layout.addWidget(self.run_number, 0, 1)

        # Update widget.
        self.update_widget = QtGui.QPushButton('Update')
        layout.addWidget(self.update_widget, 1, 1)

        # Close widget.
        self.close_button = QtGui.QPushButton('Close All')
        layout.addWidget(self.close_button, 2, 1)

        # Save button.
        self.save_button = QtGui.QPushButton('Save run statistics')
        layout.addWidget(self.save_button, 3, 1)

        # Plot runs button.
        self.plot_runs_button = QtGui.QPushButton('Plot all runs')
        layout.addWidget(self.plot_runs_button, 4, 1)
