"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""
import paths
import numpy
import platform


# Import third party libs.
from pyqtgraph.Qt import QtGui


class InputPathsWidget(QtGui.QWidget):


    def __init__(self,**kwargs):

        super(InputPathsWidget, self).__init__()

        # Define layout.
        layout = QtGui.QGridLayout()
        self.setLayout(layout)

        root_input_label = QtGui.QLabel("Root data directory")
        self.root_input_dir = QtGui.QLineEdit(text='/home/grotec/Work/XFEL/Codes/hedda/unittest/TestFiles')
        self.browse_button = QtGui.QPushButton("Browse")
        ccd1_input_label = QtGui.QLabel("CCD1 (90^o) data directory")
        self.ccd1_input_dir = QtGui.QLineEdit(text='CCD1')
        ccd2_input_label = QtGui.QLabel("CCD2 (40^o) data directory")
        self.ccd2_input_dir = QtGui.QLineEdit(text='CCD2')
        scope_input_label = QtGui.QLabel("Scope data directory")
        self.scope_input_dir = QtGui.QLineEdit('Scope')
        diode_array_input_label = QtGui.QLabel("Diode Array data directory")
        self.diode_array_input_dir = QtGui.QLineEdit('DiodeArray')
        log_input_label = QtGui.QLabel("Log directory")
        self.log_input_dir = QtGui.QLineEdit('Log')


        layout.addWidget(root_input_label, 0, 0)
        layout.addWidget(self.root_input_dir, 0, 1)
        layout.addWidget(self.browse_button, 0, 2)
        layout.addWidget(ccd1_input_label, 1, 0)
        layout.addWidget(self.ccd1_input_dir, 1, 1)
        layout.addWidget(ccd2_input_label, 2, 0)
        layout.addWidget(self.ccd2_input_dir, 2, 1)
        layout.addWidget(scope_input_label, 3, 0)
        layout.addWidget(self.scope_input_dir, 3, 1)
        layout.addWidget(diode_array_input_label, 4, 0)
        layout.addWidget(self.diode_array_input_dir, 4, 1)
        layout.addWidget(log_input_label, 5, 0)
        layout.addWidget(self.log_input_dir, 5, 1)

        self.browse_button.clicked.connect(self.launchBrowser)

    def launchBrowser(self):
        """ Opens a file browser window to select the common part of input file paths. """

        #file_dialog = QtGui.QFileDialog()
        #file_dialog.setFileMode(file_dialog.Directory)
        self.root_input_dir.setText(QtGui.QFileDialog.getExistingDirectory(self, "Select root data directory",
                                                                           "",
                                                                           QtGui.QFileDialog.ShowDirsOnly)
                                                                           )
