"""
    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""

import paths

# Import third party libs.
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui

from StatisticsUtilities import statistics

import numpy


class CCDWidget(QtGui.QWidget):

    def __init__(self, data=None):
        ######################################
        #    CCD   widget                    #
        ######################################
        super(CCDWidget, self).__init__()
        layout = QtGui.QGridLayout()
        self.setLayout(layout)

        self.frames_view = pg.ImageView(view=pg.PlotItem(title="Frame"))
        self.frames_lineout_widget = pg.PlotWidget(name='frames_lineout_widget', title="Mie Lineout")
        self.frames_lineout_widget_2 = pg.PlotWidget(name='frames_lineout_widget_2', title="Thomson Lineout")
        self.contrast_widget = pg.PlotWidget(name='contrast_widget', title="Mie/Thomson contrast")

        layout.addWidget(self.frames_view, 0, 0)
        layout.addWidget(self.frames_lineout_widget, 2,0)
        layout.addWidget(self.contrast_widget, 2,2)
        layout.addWidget(self.frames_lineout_widget_2, 0,2)
        layout.setRowMinimumHeight(0, 200)

        # Filter those frames that have negative or zero intensity.
        #frame_intensities = numpy.sum(data, axis=(1,2))
        #self.data = numpy.array([data[i] for i in range(data.shape[0]) if frame_intensities[i] > 0.0])
        if data is not None:
            self.setData(data)
            self.update()

        self.frame_roi = self.frames_view.roi
        self.frame_roi.setPen(pg.mkPen(color='g') )
        self.frame_roi.setPos([0, 0])
        self.frame_roi.setSize([10, 10])

        # Add scale handles on all corners and edges.
        self.frame_roi.addScaleHandle([0.5, 0.0], [0.5, 1.0] )
        self.frame_roi.addScaleHandle([0.0, 0.5], [1.0, 0.5] )
        self.frame_roi.addScaleHandle([0.5, 1.0], [0.5, 0.0] )
        self.frame_roi.addScaleHandle([1.0, 0.5], [0.0, 0.5] )

        self.frame_roi.translateSnap=True
        self.frame_roi.scaleSnap=True


        self.bg_roi = pg.ROI([0, 0], [10, 10], pen=(0,9))
        self.frames_view.addItem(self.bg_roi)

        self.bg_roi.addScaleHandle([0.5, 0.0], [0.5, 1.0] )
        self.bg_roi.addScaleHandle([0.0, 0.5], [1.0, 0.5] )
        self.bg_roi.addScaleHandle([0.5, 1.0], [0.5, 0.0] )
        self.bg_roi.addScaleHandle([1.0, 0.5], [0.0, 0.5] )

        self.bg_roi.translateSnap=True
        self.bg_roi.scaleSnap=True

        self.bg_roi.show()
        self.frame_roi.show()

        #self.frame_roi.sigRegionChanged.connect(self.updateLineout)
        #self.bg_roi.sigRegionChanged.connect(self.updateLineout)
        #self.frame_roi.sigRegionChanged.connect(self.getStatistics)
        #self.frames_view.sigTimeChanged.connect(self.updateLineout)

    def setData(self, data):
        if len(data.shape) < 3:
            self.data = numpy.array([data])
        else:
            self.data = data
        self.avg = numpy.mean(self.data, axis=0)

    def update(self):
        ## Display the data.
        xvals = numpy.arange(self.data.shape[0])
        self.frames_view.setImage(self.data, xvals=xvals)
        #self.updateLineout()


    def updateLineout(self):
        #global

        lineout_axis = 0
        index = self.frames_view.currentIndex
        frame_data = self.frames_view.getProcessedImage()[index]
        frame_image = self.frames_view.getImageItem()
        frame_mie_selected = self.frame_roi.getArrayRegion(frame_data, frame_image)
        frame_mie_mean = frame_mie_selected.mean(axis=lineout_axis)
        frame_thomson_selected = self.bg_roi.getArrayRegion(frame_data, frame_image)
        frame_thomson_mean = frame_thomson_selected.mean(axis=lineout_axis)
        avg_mie_selected = self.frame_roi.getArrayRegion(self.avg, frame_image)
        avg_mie_mean = avg_mie_selected.mean(axis=lineout_axis)
        avg_thomson_selected = self.bg_roi.getArrayRegion(self.avg, frame_image)
        avg_thomson_mean = avg_thomson_selected.mean(axis=lineout_axis)
        frame_label = 'Frame %d' % (index)

        # Clear plots.
        self.frames_lineout_widget.clear()
        self.frames_lineout_widget_2.clear()

        if hasattr(self.frames_lineout_widget.plotItem.legend, 'items'):
            self.frames_lineout_widget.plotItem.legend.items = []
        if hasattr(self.frames_lineout_widget_2.plotItem.legend, 'items'):
            self.frames_lineout_widget_2.plotItem.legend.items = []

        self.frames_lineout_widget.addLegend()
        self.frames_lineout_widget_2.addLegend()
        self.frames_lineout_widget.plot(frame_mie_selected.mean(axis=lineout_axis), clear=True, name=frame_label, pen=(0,255,0))
        self.frames_lineout_widget_2.plot(frame_thomson_selected.mean(axis=lineout_axis), clear=True, name=frame_label, pen=(255,0,0))
        self.contrast_widget.plot( frame_mie_mean  / frame_thomson_mean , clear=True, name=frame_label, pen=(0,0,255))
        self.contrast_widget.plot( avg_mie_mean  / avg_thomson_mean , clear=False, name=frame_label)

        self.frames_lineout_widget.plot(avg_mie_selected.mean(axis=lineout_axis), clear=False, name='avg')
        self.frames_lineout_widget_2.plot(avg_thomson_selected.mean(axis=lineout_axis), clear=False, name='avg')


    def getStatistics(self, roi=None):

        # Get Mie statistics.
        if roi is None:
            roi = self.frame_roi

        frame_data = self.frames_view.getProcessedImage()
        frame_image = self.frames_view.getImageItem()
        roi_data = roi.getArrayRegion(frame_data, frame_image, axes=(1,2))

        return statistics(roi_data)

