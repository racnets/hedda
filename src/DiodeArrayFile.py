""" Module for the DiodeArrayFile object.

    @author : CFG
    @institution : XFEL
    @creation 20151026

    @note : This file is part of HEDDA.

    HEDDA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HEDDA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HEDDA.  If not, see <http://www.gnu.org/licenses/>.
"""



import os
import numpy


class DiodeArrayFile(object):
    """
    Class representing data from a diode array stored in ascii file format.
    """

    def __init__(self, path=None):
        """
        Constructor for the DiodeArrayFile object.

        @param path : The path to the underlying file holding the diode array data.
        @type : string
        @default : None
        """

        # Check path is a valid file.
        if not os.path.isfile(path):
            raise RuntimeError('%s is not a valid file.' % (path) )
        self.__path = path

        content = open(path).readlines()

        # Replace comma by decimal point.

        # Setup arrays that hold the data.
        num_lines = len(content)
        diode_data_1 = numpy.empty( (num_lines, 8) )
        diode_data_2 = numpy.empty( (num_lines, 8) )

        # Loop over all lines.
        for l, line in enumerate(content):
            split_line = line.replace(',', '.').split()
            # Loop through the 8 diodes.
            for d in range(1,9):
                diode_data_1[l,d-1] = split_line[d]
                diode_data_2[l,d-1] = split_line[d+8]


        self.__data_1 = diode_data_1
        self.__data_2 = diode_data_2

    def getData(self):
        """ Query method for the data array. """
        return [self.__data_1, self.__data_2]

    @property
    def data(self):
        """ Query for the underlying data array. """
        return [self.__data_1, self.__data_2]

    def getMeanData(self):
        """ Query for the averaged data. """

        # Calculate average over all samples.
        return [self.__data_1.mean(axis=0), self.__data_2.mean(axis=0)]

    def getRMS(self):
        """ Query for the rms data over all samples. """

        return [self.__data_1.std(axis=0), self.__data_2.std(axis=0)]
